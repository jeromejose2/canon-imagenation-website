<?php

/*
|--------------------------------------------------------------------------
| Application Constants
|--------------------------------------------------------------------------
*/


/*
|--------------------------------------------------------------------------
| Content Management System Constants
|--------------------------------------------------------------------------
*/

# PAGINATION
define('PER_PAGE', isset($_GET['per_page']) ? $_GET['per_page'] : 15);

# PHOTOS
define('SHARING', 1);
define('ENTRY', 2);

# BITLY API PROVIDER
define('BITLY_LOGIN', 'jeffreymabazza');
define('BITLY_API_KEY', 'R_282f6f5aa77746059254a00858a0ac3c');
define('BITLY_URL', "http://api.bitly.com/v3/shorten?format=json&apiKey=". BITLY_API_KEY ."&login=". BITLY_LOGIN ."&longUrl=");

# ENVIRONMENT
define('APP_ENV', getenv('APP_ENV') ?: 'production');