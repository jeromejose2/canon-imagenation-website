<?php

/*
|--------------------------------------------------------------------------
| RESTful Routes
|--------------------------------------------------------------------------
*/

Route::controller('api', 'ApiController');



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/

### Homepage ###
Route::group([], function() {
	Route::get('/', 'HomeController@index');
});

### Socialite ###
Route::group(['prefix'=>'facebook'], function() {
	Route::get('authenticate', 'SocialiteController@redirectToProvider');
	Route::get('handle_callback', 'SocialiteController@handleProviderCallback');
	Route::get('create_fake_session', 'SocialiteController@create_fake_session');
});

### Submit Entry ###
Route::post('submit-entry', 'HomeController@submit');

### AJAX ###
Route::group(['prefix'=>'ajax'], function() {
	Route::get('photos', 'AjaxController@photo');
	Route::get('feeds', 'AjaxController@feed');
	Route::post('subscribe', 'AjaxController@subscribe');
});

### Blogs ###
Route::group(['prefix'=>'blogs'], function() {
	Route::get('/', 'FeedController@index');
	Route::get('read/{permalink}', 'FeedController@show');
	Route::get('permalink', 'FeedController@index');
	Route::get('some', 'FeedController@show');
});

### Subscribe ###
Route::post('subscribe', 'SubscribeController@store');

### Popups ###
Route::group(['prefix'=>'popups'], function() {
	Route::get('thank_you', 'PopupController@thank_you');
	Route::get('login', 'PopupController@login');
	Route::get('mechanics', 'PopupController@mechanics');
	Route::get('photo', 'PopupController@photo');
	Route::get('terms', 'PopupController@terms');
	Route::get('upload', 'PopupController@upload');
	Route::get('upload-with-existing-user', 'PopupController@upload_with_existing_user');
	Route::get('invalid_submission', 'PopupController@invalid_submission');
	Route::get('subscription_error', 'PopupController@subscription_error');
});

### Log Out ###
Route::get('logout', 'SocialiteController@logout');

/*
|--------------------------------------------------------------------------
| CMS Routes
|--------------------------------------------------------------------------
*/
### Default ###
Route::get('admin', function() {
	return Redirect::to('admin/login');
});

### Login ###
Route::get('admin/login', 'Admin\LoginController@index');
Route::post('admin/auth', 'Admin\LoginController@authenticate');

Route::group(['prefix'=>'admin', 'middleware'=>'route'], function() {
	### Dashboard ###
	Route::get('dashboard', 'Admin\DashboardController@index');
	Route::get('graph/{year}', 'Admin\DashboardController@filter_graph');

	### Users ###
	Route::group(['prefix'=>'users'], function() {
		Route::get('/', 'Admin\UsersController@index');
		Route::get('export', 'Admin\UsersController@export');
	});

	### Photos ###
	Route::group(['prefix'=>'user_photos'], function() {
		Route::get('/', 'Admin\PhotosController@index');
		Route::get('approve/{id}', 'Admin\PhotosController@approve');
		Route::get('disapprove/{id}', 'Admin\PhotosController@disapprove');
		Route::get('export', 'Admin\PhotosController@export');
	});

	### Feeds ###
	Route::group(['prefix'=>'feeds'], function() {
		Route::get('/', 'Admin\FeedsController@index');
		Route::get('destroy/{id}', 'Admin\FeedsController@destroy');
		Route::get('create', 'Admin\FeedsController@create');
		Route::get('edit/{id}', 'Admin\FeedsController@edit');
		Route::get('export', 'Admin\FeedsController@export');
		Route::post('store', 'Admin\FeedsController@store');
		Route::post('update', 'Admin\FeedsController@update');
	});

	### Contest ###
	Route::group(['prefix'=>'contest'], function() {
		Route::get('/', 'Admin\ContestController@index');
		Route::get('edit/{id}', 'Admin\ContestController@edit');
		Route::get('destroy/{id}', 'Admin\ContestController@destroy');
		Route::post('update', 'Admin\ContestController@update');
	});

	### Logs ###
	Route::group(['prefix'=>'logs'], function() {
		Route::get('/', 'Admin\AuditController@index');
		Route::get('export', 'Admin\AuditController@export');
	});

	### Account Settings ###
	Route::group(['prefix'=>'account'], function() {
		Route::get('settings', 'Admin\AccountsController@index');
		Route::post('update', 'Admin\AccountsController@update');
	});

	### Subscribers ###
	Route::group(['prefix'=>'subscribers'], function() {
		Route::get('/', 'Admin\SubscribersController@index');
		Route::get('export', 'Admin\SubscribersController@export');
	});

	### Logout ###
	Route::get('logout', 'Admin\LoginController@destroy');
});