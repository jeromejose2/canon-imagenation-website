<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use View;
use Request;
use Redirect;

use App\Feed as Blog;

class FeedController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$blogs = Blog::where('is_deleted', '=', 0)->orderBy('date', 'DESC')->get();

		return View::make('app.pages.blog')->with(compact('blogs'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($permalink)
	{
		$blog = Blog::where('is_deleted', '=', 0)->where('permalink', '=', $permalink)->first();

		if( ! $blog) {
			return Redirect::to('blogs');
		}

		return View::make('app.pages.blog-inner')->with(compact('blog'));
	}

}
