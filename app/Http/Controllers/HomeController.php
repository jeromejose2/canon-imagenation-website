<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use View;
use Request;
use Validator;
use Input;
use Session;
use Eventviva;
use Redirect;
use Socialite;

use App\Contest;
use App\Feed;
use App\Photo;
use App\Category;
use App\User;

class HomeController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$feeds = Feed::where('is_deleted', '='. 0)->orderBy('date', 'DESC')->paginate(3);
		$contest = Contest::get()->first();
		$photos = Photo::where('status', '=', 1)->paginate(9);
		$categories = Category::get();

		return View::make('app.pages.index')->with(compact('feeds', 'contest', 'photos', 'categories'));
	}

	public function submit()
	{
		if( ! Session::has('facebook_user')) {
			return Redirect::to('/')->with('invalid_submission', '1');
		}

		$valid_user = User::find(Session::get('facebook_user')['user_id']);

		if( ! $valid_user) {
			return Redirect::to('/')->with('invalid_submission', '1');
		}

		$rules = [
			'title'=>'required',
			'description'=>'required',
			'camera'=>'required',
			'settings_iso'=>'required',
			'settings_aperture'=>'required',
			'settings_shutter_speed'=>'required',
			'category'=>'required',
			'type'=>'required',
			'terms_and_conditions'=>'required',
			'files'=>'required',
			'g-recaptcha-response'=>'required'
		];

		$validator = Validator::make(Request::except('_token'), $rules);

		if($validator->fails()) {
			$messages = $validator->messages();
			foreach($messages->all() as $message) {
				print_r($message);
			}
			exit;
		}

		$destination = 'uploads';
		foreach(Request::file('files') as $file) {
			$filename = md5(date('us').$file->getClientOriginalName()).'.'.$file->getClientOriginalExtension();
			$file->move($destination, $filename);

			$image = new Eventviva('./uploads/'.$filename);
			$image->resizeToWidth(300);
			$image->save('./uploads/thumb_'.$filename);

			$image = new Eventviva('./uploads/'.$filename);
			$image->resizeToWidth(500);
			$image->save('./uploads/cms_'.$filename);

			$photo = new Photo;

			$photo->user_id = Session::get('facebook_user')['user_id'];
			$photo->photo = $filename;
			$photo->title = Request::input('title');
			$photo->description = Request::input('description');
			$photo->camera = Request::input('camera');
			$photo->category = Request::input('category');
			$photo->status = 0;
			$photo->type = Request::input('type');

			$photo->settings = serialize([
				'iso'=>Request::input('settings_iso'),
				'aperture'=>Request::input('settings_aperture'),
				'shutter_speed'=>Request::input('settings_shutter_speed')
			]);

			$photo->save();
		}

		return Redirect::to('/')->with('has_thankyou', 'Thank you for uploading your photo! Our judges will be reviewing your entry!');
	}

}
