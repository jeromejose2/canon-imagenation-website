<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Admin\Controller;

use View;
use DB;

use App\Photo;

class DashboardController extends Controller {

	/**
	 * Display the dashboard
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = [];
		$data[1] = ['month'=>'January', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '01')->where(DB::raw("YEAR(created_at)"), '=', '2015')->count()];
		$data[2] = ['month'=>'February', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '02')->where(DB::raw("YEAR(created_at)"), '=', '2015')->count()];
		$data[3] = ['month'=>'March', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '03')->where(DB::raw("YEAR(created_at)"), '=', '2015')->count()];
		$data[4] = ['month'=>'April', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '04')->where(DB::raw("YEAR(created_at)"), '=', '2015')->count()];
		$data[5] = ['month'=>'May', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '05')->where(DB::raw("YEAR(created_at)"), '=', '2015')->count()];
		$data[6] = ['month'=>'June', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '06')->where(DB::raw("YEAR(created_at)"), '=', '2015')->count()];
		$data[7] = ['month'=>'July', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '07')->where(DB::raw("YEAR(created_at)"), '=', '2015')->count()];
		$data[8] = ['month'=>'August', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '08')->where(DB::raw("YEAR(created_at)"), '=', '2015')->count()];
		$data[9] = ['month'=>'September', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '09')->where(DB::raw("YEAR(created_at)"), '=', '2015')->count()];
		$data[10] = ['month'=>'October', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '10')->where(DB::raw("YEAR(created_at)"), '=', '2015')->count()];
		$data[11] = ['month'=>'November', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '11')->where(DB::raw("YEAR(created_at)"), '=', '2015')->count()];
		$data[12] = ['month'=>'December', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '12')->where(DB::raw("YEAR(created_at)"), '=', '2015')->count()];

		$activities = DB::table('audits')->select(DB::raw("*, `audits`.`created_at` as audit_created_at"))->join('admins', function($query) {
			$query->on('audits.user_id', '=', 'admins.id');
		})->orderBy('audit_created_at', 'desc')->paginate(PER_PAGE);

		return View::make('admin.pages.dashboard')->with(compact('data', 'activities'));
	}

	public function filter_graph($year = 2015)
	{
		$data = [];
		$data[1] = ['month'=>'January', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '01')->where(DB::raw("YEAR(created_at)"), '=', $year)->count()];
		$data[2] = ['month'=>'February', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '02')->where(DB::raw("YEAR(created_at)"), '=', $year)->count()];
		$data[3] = ['month'=>'March', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '03')->where(DB::raw("YEAR(created_at)"), '=', $year)->count()];
		$data[4] = ['month'=>'April', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '04')->where(DB::raw("YEAR(created_at)"), '=', $year)->count()];
		$data[5] = ['month'=>'May', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '05')->where(DB::raw("YEAR(created_at)"), '=', $year)->count()];
		$data[6] = ['month'=>'June', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '06')->where(DB::raw("YEAR(created_at)"), '=', $year)->count()];
		$data[7] = ['month'=>'July', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '07')->where(DB::raw("YEAR(created_at)"), '=', $year)->count()];
		$data[8] = ['month'=>'August', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '08')->where(DB::raw("YEAR(created_at)"), '=', $year)->count()];
		$data[9] = ['month'=>'September', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '09')->where(DB::raw("YEAR(created_at)"), '=', $year)->count()];
		$data[10] = ['month'=>'October', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '10')->where(DB::raw("YEAR(created_at)"), '=', $year)->count()];
		$data[11] = ['month'=>'November', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '11')->where(DB::raw("YEAR(created_at)"), '=', $year)->count()];
		$data[12] = ['month'=>'December', 'count'=>DB::table('photos')->where(DB::raw("MONTH(created_at)"), '=', '12')->where(DB::raw("YEAR(created_at)"), '=', $year)->count()];

		return View::make('admin.pages.graph')->with(compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
