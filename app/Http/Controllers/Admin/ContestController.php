<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Admin\Controller;

use Request;
use View;
use Redirect;

use App\Contest;

class ContestController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$record = Contest::get()[0];

		return View::make('admin.pages.contest')->with(compact('record'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id = false)
	{
		$contest = Contest::find(Request::input('id'));
		$previous = is_array($contest->previous) ? unserialize($contest->previous) : [];

		if(Request::hasFile('photo')) {
			$photo = Request::file('photo');
			$original_filename = $photo->getClientOriginalName();
			$extension = $photo->getClientOriginalExtension();
			$encrypted_filename = md5(date('us').$original_filename).'.'.$extension;

			Request::file('photo')->move('assets_admin/uploads/contest', $encrypted_filename);

			$contest->photo = $encrypted_filename;
			$contest->original_filename = $original_filename;
		}

		$contest->description = Request::input('description');
		$contest->mechanics = Request::input('mechanics');
		$contest->title = Request::input('title');

		if($contest->title != Request::input('title')) {
			$contest->previous = serialize(array_merge($previous, [md5(date('us'))=>['title'=>Request::input('title'), 'updated_at'=>date('Y-m-d H:i:s')]]));
		}

		$contest->save();

		audit([
			'activity'=> "{{ name }} updated record id $contest->id in contests table",
			'method'=>'update'
		]);

		return Redirect::to('admin/contest');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
