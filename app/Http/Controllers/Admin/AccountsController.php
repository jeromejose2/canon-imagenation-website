<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Admin\Controller;

use Session;
use Request;
use View;
use Hash;
use Redirect;

use App\Admin;

class AccountsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$record = Admin::find(Session::get('admin')['id']);

		return View::make('admin.pages.accounts-manage')->with(compact('record'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$admin = Admin::find(Session::get('admin')['id']);

		$admin->username = Request::input('username');
		$admin->email = Request::input('email');
		if(Request::input('password')) {
			$admin->password = Hash::make(Request::input('password'));
		}

		$admin->save();

		Session::forget('admin');
		return Redirect::to('admin/login');
	}

}