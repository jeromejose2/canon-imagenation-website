<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Admin\Controller;

use Request;
use View;
use Excel;

use App\Subscriber;

class SubscribersController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$num = Request::input('page') ? ((Request::input('page') - 1) * PER_PAGE) + 1 : 1;
		$fragments = generate_fragments(Request::input());
		$subscribers = Subscriber::orderBy('created_at', 'DESC')->paginate(PER_PAGE);

		return View::make('admin.pages.subscribers')->with(compact('subscribers', 'num', 'fragments'));
	}

	public function export()
	{
		$subscribers = Subscriber::get();

		Excel::create('Subscribers-'.date('YdmHis'), function($excel) use($subscribers) {
			$excel->sheet('Sheetname', function($sheet) use($subscribers) {
				$sheet->row(1, ['#', 'Firstname', 'Lastname', 'Email', 'Date Created']);
				$sheet->row(2, []);
				if($subscribers) {
					$i = 3;
					foreach($subscribers as $subscriber) {
						$sheet->row($i, [
							$i - 2,
							$subscriber->first_name,
							$subscriber->last_name,
							$subscriber->email,
							date('F d, Y', strtotime($subscriber->created_at))
						]);
						$i++;
					}
				}
			});
		})->download('csv');
	}

}
