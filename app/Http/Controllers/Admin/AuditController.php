<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Admin\Controller;

use Request;
use View;
use Redirect;
use DB;
use Excel;

use App\Audit;

class AuditController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$num = Request::input('page') ? ((Request::input('page') - 1) * PER_PAGE) + 1 : 1;
		$fragments = generate_fragments(Request::input());
		$audits = DB::table('audits')->select(DB::raw("*, `audits`.`created_at` as audit_created_at"))->join('admins', function($query) {
			$query->on('audits.user_id', '=', 'admins.id');
			if(Request::input('title'))
				$query->where('username', '=', Request::input('title'));
		})->orderBy('audit_created_at', 'desc')->paginate(PER_PAGE);

		return View::make('admin.pages.audits')->with(compact('num', 'fragments', 'audits'));
	}

	public function export()
	{
		$audits = DB::table('audits')->select(DB::raw("*, `audits`.`created_at` as audit_created_at"))->join('admins', function($query) {
			$query->on('audits.user_id', '=', 'admins.id');
			if(Request::input('title'))
				$query->where('username', '=', Request::input('title'));
		})->get();

		Excel::create('Logs-'.date('YdmHis'), function($excel) use($audits) {
			$excel->sheet('Sheetname', function($sheet) use($audits) {
				$sheet->row(1, ['#', 'User ID', 'Name', 'Activity', 'Date Created']);
				$sheet->row(2, []);
				if($audits) {
					$i = 3;
					foreach($audits as $audit) {
						$sheet->row($i, [
							$i - 2,
							$audit->user_id,
							$audit->username,
							str_replace('{{ name }}', '', $audit->activity),
							date('F d, Y H:i:s', strtotime($audit->audit_created_at))
						]);
						$i++;
					}
				}
			});
		})->download('csv');
	}

}
