<?php namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Session;
use Redirect;
use Request;
use Excel;

use App\Audit;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;

	public function __construct()
	{
		if(Request::segment(2) == null) {
			header('Location: login');
		}

		if(Request::segment(2) == 'login' && Session::get('admin')) {
			header('Location: dashboard');
		}

		if(Request::segment(2) != 'login' && Request::segment(2) != 'auth' && Request::segment(2) != 'logout') {
			if( ! Session::get('admin')) {
				header('Location: login');
			}
		}

		function audit($params)
		{
			$audit = new Audit;

			$audit->user_id = Session::get('admin')['id'];
			if(isset($params['activity']))
				$audit->activity = $params['activity'];
			if(isset($params['method']))
				$audit->method = $params['method'];

			$audit->save();
		}

		function generate_fragments($params)
		{
			$fragments = [];

			if(is_array($params)) {
				foreach(array_except($params, 'page') as $fragment_key => $fragment_value) {
					if($fragment_value) {
						$fragments[$fragment_key] = $fragment_value;
					}
				}
			}

			return $fragments;
		}
	}

}