<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Admin\Controller;

use View;
use Request;
use DB;
use Excel;

use App\User;
use App\Photo;

class UsersController extends Controller {

	/**
	 * Show the list of Users
	 *
	 * @return Response
	 */
	public function index()
	{
		$num = Request::input('page') ? ((Request::input('page') - 1) * PER_PAGE) + 1 : 1;
		$fragments = generate_fragments(Request::input());
		$users = User::where(function($query) {
			if(Request::input('first_name'))
				$query->where('first_name', 'LIKE', '%'.Request::input('first_name').'%');
			if(Request::input('last_name'))
				$query->where('last_name', 'LIKE', '%'.Request::input('last_name').'%');
			if(Request::input('facebook_id'))
				$query->where('facebook_id', 'LIKE', '%'.Request::input('facebook_id').'%');
		})->orderBy('created_at', 'DESC')->paginate(PER_PAGE);

		// dd($users);

		if($users) {
			foreach($users as $user) {
				$has_photo = Photo::where('user_id', '=', $user->id)->first();
				if($has_photo) {
					$user->has_photo = true;
				}
			}
		}

		return View::make('admin.pages.users')->with(compact('num', 'users', 'fragments'));
	}

	public function export()
	{
		$users = User::where(function($query) {
			if(Request::input('first_name'))
				$query->where('first_name', 'LIKE', '%'.Request::input('first_name').'%');
			if(Request::input('last_name'))
				$query->where('last_name', 'LIKE', '%'.Request::input('last_name').'%');
			if(Request::input('facebook_id'))
				$query->where('facebook_id', 'LIKE', '%'.Request::input('facebook_id').'%');
		})->get();

		Excel::create('Users-'.date('YdmHis'), function($excel) use($users) {
			$excel->sheet('Sheetname', function($sheet) use($users) {
				$sheet->row(1, ['#', 'Facebook ID', 'Firstname', 'Lastname', 'Email', 'Gender', 'Date Created']);
				$sheet->row(2, []);
				if($users) {
					$i = 3;
					foreach($users as $user) {
						$sheet->row($i, [
							$i - 2,
							$user->facebook_id,
							$user->first_name,
							$user->last_name,
							$user->email,
							$user->gender,
							date('F d, Y', strtotime($user->created_at))
						]);
						$i++;
					}
				}
			});
		})->download('csv');
	}

}