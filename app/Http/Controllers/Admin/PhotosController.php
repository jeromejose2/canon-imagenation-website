<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Admin\Controller;

use View;
use Request;
use DB;
use Redirect;
use Excel;

use App\Photo;
use App\Category;

class PhotosController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$num = Request::input('page') ? ((Request::input('page') - 1) * PER_PAGE) + 1 : 1;
		$fragments = generate_fragments(Request::input());
		$photos = DB::table('photos')->select(DB::raw("*, `photos`.`id` as photo_id, photos.created_at as date_published, CONCAT(`users`.`first_name`, ' ', `users`.`last_name`) as name"))->join('users', function($query){
			$query->on('photos.user_id', '=', 'users.id');
			if(Request::input('user_id'))
				$query->where('user_id', '=', Request::input('user_id'));
			if(Request::input('name'))
				$query->where(DB::raw("CONCAT(`users`.`first_name`, ' ', `users`.`last_name`)"), 'LIKE', '%'.Request::input('name').'%');
			if(Request::input('title'))
				$query->where('title', 'LIKE', '%'.Request::input('title').'%');
			if(Request::input('description'))
				$query->where('description', 'LIKE', '%'.Request::input('description').'%');
			if(Request::input('camera'))
				$query->where('camera', 'LIKE', '%'.Request::input('camera').'%');
			if(Request::input('category'))
				$query->where('category', 'LIKE', '%'.Request::input('category').'%');
			if(Request::input('type'))
				$query->where('type', '=', Request::input('type'));
			if(Request::input('from'))
				$query->where(DB::raw('DATE(photos.created_at)'), '>=', date('Y-m-d', strtotime(Request::input('from'))));
			if(Request::input('to'))
				$query->where(DB::raw('DATE(photos.created_at)'), '<=', date('Y-m-d', strtotime(Request::input('to'))));
		})->orderBy('photos.created_at', 'desc')->paginate(PER_PAGE);
		$categories = Category::all();

		return View::make('admin.pages.photos')->with(compact('num', 'photos', 'fragments', 'categories'));
	}

	/**
	 * Update a record's status
	 *
	 * @param  [int] $id [Photo's id]
	 * @return Response
	 */
	public function approve($id)
	{
		if( ! is_numeric($id)) {
			return Redirect::to('admin/user_photos')->with('error', 'Sorry, the ID you provided is invalid.');
		}

		$photo = Photo::find($id);
		if( ! $photo) {
			return Redirect::to('admin/user_photos')->with('error', 'Sorry, record does not exist.');
		}

		if($photo->status == 1) {
			return Redirect::to('admin/user_photos')->with('error', 'Sorry, record is already approved.');
		}

		$photo->status = 1;
		$photo->save();

		audit([
			'activity'=>"{{ name }} approved record id $id in photos table",
			'method'=>'update'
		]);

		return Redirect::back();
	}

	public function disapprove($id)
	{
		if( ! is_numeric($id)) {
			return Redirect::to('admin/user_photos')->with('error', 'Sorry, the ID you provided is invalid.');
		}

		$photo = Photo::find($id);
		if( ! $photo) {
			return Redirect::to('admin/user_photos')->with('error', 'Sorry, record does not exist.');
		}

		if($photo->status == 2) {
			return Redirect::to('admin/user_photos')->with('error', 'Sorry, record is already disapproved.');
		}

		$photo->status = 2;
		$photo->save();

		audit([
			'activity'=>"{{ name }} disapproved record id $id in photos table",
			'method'=>'update'
		]);

		return Redirect::back();
	}

	/**
	 * Export data into csv format
	 *
	 * @return CSV
	 */
	public function export()
	{
		$photos = DB::table('photos')->select(DB::raw("*, photos.created_at as date_published, CONCAT(`users`.`first_name`, ' ', `users`.`last_name`) as name"))->join('users', function($query){
			$query->on('photos.user_id', '=', 'users.id');
			if(Request::input('name'))
				$query->where(DB::raw("CONCAT(`users`.`first_name`, ' ', `users`.`last_name`)"), 'LIKE', '%'.Request::input('name').'%');
			if(Request::input('title'))
				$query->where('title', 'LIKE', '%'.Request::input('title').'%');
			if(Request::input('description'))
				$query->where('description', 'LIKE', '%'.Request::input('description').'%');
			if(Request::input('camera'))
				$query->where('camera', 'LIKE', '%'.Request::input('camera').'%');
			if(Request::input('category'))
				$query->where('category', 'LIKE', '%'.Request::input('category').'%');
			if(Request::input('type'))
				$query->where('type', '=', Request::input('type'));
			if(Request::input('from'))
				$query->where(DB::raw('DATE(photos.created_at)'), '>=', date('Y-m-d', strtotime(Request::input('from'))));
			if(Request::input('to'))
				$query->where(DB::raw('DATE(photos.created_at)'), '<=', date('Y-m-d', strtotime(Request::input('to'))));
		})->get();

		Excel::create('Photos-'.date('YdmHis'), function($excel) use($photos) {
			$excel->sheet('Sheetname', function($sheet) use($photos) {
				$sheet->row(1, ['#', 'Name', 'Title', 'Description', 'Camera', 'Category', 'Date Created']);
				$sheet->row(2, []);
				if($photos) {
					$i = 3;
					foreach($photos as $photo) {
						$sheet->row($i, [
							$i - 2,
							$photo->name,
							$photo->title,
							$photo->description,
							$photo->camera,
							$photo->category,
							date('F d, Y', strtotime($photo->created_at))
						]);
						$i++;
					}
				}
			});
		})->download('csv');
	}

}
