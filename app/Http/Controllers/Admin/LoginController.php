<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Admin\Controller;

use Request;
use View;
use Validator;
use Redirect;
use Session;
use Hash;

use App\Admin;

class LoginController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('admin.layouts.login');
	}

	/**
	 * Authenticate the user
	 *
	 * @return Response
	 */
	public function authenticate()
	{
		$validator = Validator::make(
			[
				'email'=>Request::input('email'),
				'password'=>Request::input('password')
			],
			[
				'email'=>'required|email',
				'password'=>'required'
			],
			[
				'required'=>'Please enter you :attribute to proceed.',
				'email'=>'Please enter a valid email address to proceed.'
			]
		);

		if($validator->fails()) {
			return Redirect::to('admin/login')->with('error', current($validator->messages()->all()));
		}

		$user = Admin::where('email', 'LIKE', '%'.Request::input('email').'%')->first();

		if( ! $user) {
			return Redirect::to('admin/login')->with('error', 'Sorry, the email you have entered does not exist.');
		}

		if( ! Hash::check(Request::input('password'), $user->password)) {
			return Redirect::to('admin/login')->with('error', 'Sorry, the password you have entered is invalid.');
		}

		Session::put('admin', [
			'id'=>$user->id,
			'username'=>$user->username,
			'role'=>$user->role
		]);

		audit([
			'activity'=>"{{ name }} user id $user->id has logged in",
			'method'=>'login'
		]);

		return Redirect::to('admin/dashboard');
	}

	/**
	 * Redirect to admin/login when visiting admin
	 *
	 * @return response
	 */
	public function fix()
	{
		return Redirect::to('admin/login');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the session
	 *
	 * @return Response
	 */
	public function destroy()
	{
		if(Session::get('admin')) {
			Session::forget('admin');
		}

		return Redirect::to('admin/login');
	}

}
