<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Admin\Controller;

use View;
use Request;
use Redirect;
use Session;
use Excel;

use Mremi\UrlShortener\Model\Link;
use Mremi\UrlShortener\Provider\Bitly\BitlyProvider;
use Mremi\UrlShortener\Provider\Bitly\OAuthClient;
use Mremi\UrlShortener\Provider\Bitly\GenericAccessTokenAuthenticator;

use App\Feed;

class FeedsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$num = Request::input('page') ? ((Request::input('page') - 1) * PER_PAGE) + 1 : 1;
		$fragments = generate_fragments(Request::input());
		$feeds = Feed::where(function($query) {
			$query->where('is_deleted', '=', 0);
			if(Request::input('title'))
				$query->where('title', 'LIKE', '%'.Request::input('title').'%');
			if(Request::input('author'))
				$query->where('crusader', 'LIKE', '%'.Request::input('author').'%');
		})->orderBy('created_at', 'DESC')->paginate(PER_PAGE);

		return View::make('admin.pages.feeds')->with(compact('num', 'feeds', 'fragments'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$method = 'create';

		return View::make('admin.pages.feeds-manage')->with(compact('method'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if(Request::except('_token')) {
			$feed = new Feed;

			if(Request::hasFile('photo')) {
				$photo = Request::file('photo');
				$original_filename = $photo->getClientOriginalName();
				$extension = $photo->getClientOriginalExtension();
				$encrypted_filename = md5(date('us').$original_filename).'.'.$extension;

				Request::file('photo')->move('assets_admin/uploads/blog', $encrypted_filename);

				$feed->photo = $encrypted_filename;
				$feed->original_filename = $original_filename;
			}

			$permalink = $this->create_permalink(Request::input('title'));

			$feed->link = Request::input('link');
			$feed->crusader = Request::input('crusader');
			$feed->title = Request::input('title');
			$feed->date = Request::input('date');
			$feed->description = Request::input('description');
			$feed->permalink = $permalink;
			$feed->short_url = $this->create_bitly(url().'/blogs/read/'.$permalink);

			$feed->save();

			audit([
				'activity'=>"{{ name }} created record id $feed->id in feeds table",
				'method'=>'create'
			]);
		}

		return Redirect::to('admin/feeds');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if( ! is_numeric($id)) {
			return Redirect::to('admin/feeds')->with('error', 'Sorry, the ID you provided is invalid.');
		}

		$record = Feed::find($id);
		if( ! $record) {
			return Redirect::to('admin/feeds')->with('error', 'Sorry, record does not exist.');
		}

		if($record->is_deleted == 1) {
			return Redirect::to('admin/feeds')->with('error', 'Sorry, record is already deleted.');
		}
		$method = 'Edit';

		audit([
			'activity'=>"{{ name }} updated record id $id in feeds table",
			'method'=>'update'
		]);

		return View::make('admin.pages.feeds-manage')->with(compact('record', 'method'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @return Response
	 */
	public function update()
	{
		$feed = Feed::find(Request::input('id'));

		if( ! $feed) {
			return Redirect::to('admin/feeds')->with('error', 'Sorry, record does not exist.');
		}

		if($feed->is_deleted == 1) {
			return Redirect::to('admin/feeds')->with('error', 'Sorry, record is already deleted.');
		}

		// if(Request::except('_token')) {
		// 	$permalink = $this->create_permalink(Request::input('title'));
		// 	foreach(Request::except('_token') as $key => $value) {
		// 		$feed->$key = $value;
		// 	}
		// 	$feed->permalink = $permalink;
		// 	$feed->short_url = $this->create_bitly(url().'/blogs/read/'.$permalink);

		// 	$feed->save();
		// }

		if(Request::hasFile('photo')) {
			$photo = Request::file('photo');
			$original_filename = $photo->getClientOriginalName();
			$extension = $photo->getClientOriginalExtension();
			$encrypted_filename = md5(date('us').$original_filename).'.'.$extension;

			Request::file('photo')->move('assets_admin/uploads/blog', $encrypted_filename);

			$feed->photo = $encrypted_filename;
			$feed->original_filename = $original_filename;
		}

		$permalink = $this->create_permalink(Request::input('title'));

		$feed->link = Request::input('link');
		$feed->crusader = Request::input('crusader');
		$feed->title = Request::input('title');
		$feed->date = Request::input('date');
		$feed->description = Request::input('description');
		$feed->permalink = $permalink;
		$feed->short_url = $this->create_bitly(url().'/blogs/read/'.$permalink);

		$feed->save();

		return Redirect::to('admin/feeds');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if( ! is_numeric($id)) {
			return Redirect::to('admin/feeds')->with('error', 'Sorry, the ID you provided is invalid.');
		}

		$feed = Feed::find($id);
		if( ! $feed) {
			return Redirect::to('admin/feeds')->with('error', 'Sorry, record does not exist.');
		}

		if($feed->is_deleted == 1) {
			return Redirect::to('admin/feeds')->with('error', 'Sorry, record is already deleted.');
		}

		$feed->is_deleted = 1;
		$feed->update();

		audit([
			'activity'=>"{{ name }} deleted record id $id in feeds table",
			'method'=>'delete'
		]);

		return Redirect::back();
	}

	public function create_permalink($str, $replace=array(), $delimiter='-')
	{
		setlocale(LC_ALL, 'en_US.UTF8');
		if( !empty($replace) ) {
			$str = str_replace((array)$replace, ' ', $str);
		}

		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

		return $clean;
	}

	public function create_bitly($url)
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, BITLY_URL . $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$data = curl_exec($curl);
		curl_close($curl);
		$data = json_decode($data, true);

		return $data['data']['url'];
	}

	public function export()
	{
		$feeds = Feed::where(function($query) {
			$query->where('is_deleted', '=', 0);
			if(Request::input('title'))
				$query->where('title', 'LIKE', '%'.Request::input('title').'%');
			if(Request::input('author'))
				$query->where('crusader', 'LIKE', '%'.Request::input('author').'%');
		})->get();

		Excel::create('Feeds-'.date('YdmHis'), function($excel) use($feeds) {
			$excel->sheet('Sheetname', function($sheet) use($feeds) {
				$sheet->row(1, ['#', 'Title', 'Author', 'Description', 'Date Publish', 'Date Created']);
				$sheet->row(2, []);
				if($feeds) {
					$i = 3;
					foreach($feeds as $feed) {
						$sheet->row($i, [
							$i - 2,
							$feed->title,
							$feed->crusader,
							$feed->description,
							date('F d, Y', strtotime($feed->date)),
							date('F d, Y', strtotime($feed->created_at))
						]);
						$i++;
					}
				}
			});
		})->download('csv');
	}

}
