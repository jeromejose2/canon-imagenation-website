<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Category;
use App\Contest;
use App\Feed;
use App\Photo;
use App\User;

class ApiController extends Controller {


	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	public function __construct()
	{
	}


	/*
	|--------------------------------------------------------------------------
	| Articles
	|--------------------------------------------------------------------------
	*/

	public function getArticles()
	{
		$data = [];
		$data['success'] = true;
		$data['error'] = '';

		$per_page = isset($_GET['per_page']) && is_numeric($_GET['per_page']) ? $_GET['per_page'] : 100;

		$articles = Feed::where('is_deleted', '=', 0)->orderBy('date', 'desc')->paginate($per_page);

		$result = [];
		if($articles) {
			foreach($articles as $key => $article) {
				$result[$key]['id'] = $article->id;
				$result[$key]['photo'] = $article->photo;
				$result[$key]['description'] = $article->description;
				$result[$key]['title'] = $article->title;
				$result[$key]['author'] = $article->crusader;
				$result[$key]['permalink'] = $article->permalink;
				$result[$key]['date_publish'] = date('F j, Y', strtotime($article->date));
				$result[$key]['photo'] = $article->photo;
			}
		}

		$data['data'] = $result;

		return $data;
	}

	public function getArticle($permalink = false)
	{
		$data = [];
		$data['success'] = false;
		$data['error'] = '';

		if( ! $permalink) {
			$data['error'] = 'Permalink is required.';
			return $data;
		}

		$article = Feed::where('is_deleted', '=', 0)->where('permalink', '=', $permalink)->first();

		if( ! $data) {
			$data['success'] = true;
			$data['error'] = 'No record found.';
			return $data;
		}

		$data['success'] = true;
		$data['data'] = $article;
		return $data;
	}


	/*
	|--------------------------------------------------------------------------
	| Categories
	|--------------------------------------------------------------------------
	*/

	public function getCategories()
	{
		$data = [];
		$data['success'] = true;
		$data['error'] = '';

		$categories = Category::get();
		$data['data'] = $categories;

		return $data;
	}


	/*
	|--------------------------------------------------------------------------
	| Contest Details
	|--------------------------------------------------------------------------
	*/
	public function getContestDetails()
	{
		$data = [];
		$data['success'] = true;
		$data['error'] = '';

		$details = Contest::get()->first();
		$data['data'] = $details;

		return $data;
	}

	/*
	|--------------------------------------------------------------------------
	| Popups
	|--------------------------------------------------------------------------
	*/

	public function getPopupUpload()
	{

	}

}
