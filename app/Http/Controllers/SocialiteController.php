<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Socialite;
use Session;
use App\User;
use Auth;
use Hash;

use Laravel\Socialite\Two\User as Soc;

class SocialiteController extends Controller {

	public function redirectToProvider()
	{
		if(Session::has('facebook_user')) {
			Session::forget('facebook_user');
		}

	    return Socialite::driver('facebook')->scopes(['email', 'public_profile'])->redirect();
	}

	public function handleProviderCallback(Request $request)
	{
		$request->session()->put('state', $_GET['state']);
	    $user_details = Socialite::with('facebook')->user();

	    if($user_details) {
	    	$existing_user = User::where('facebook_id', $user_details->id)->first();

	    	if( ! $existing_user) {
	    		$user = new User;

		    	$user->facebook_id = $user_details->id;
		    	$user->first_name = $user_details->user['first_name'];
		    	$user->last_name = $user_details->user['last_name'];
		    	$user->gender = $user_details->user['gender'];
		    	$user->email = $user_details->email;
		    	$user->password = Hash::make($user_details->id);

		    	$user->save();

		    	if( ! Session::get('facebook_user')) {
		    		Session::put('facebook_user', [
		    			'user_id'=>$user->id,
		    			'facebook_id'=>$user_details->id,
		    			'first_name'=>$user_details->user['first_name'],
		    			'last_name'=>$user_details->user['last_name'],
		    			'gender'=>$user_details->user['gender'],
		    			'email'=>$user_details->email
		    		]);
		    	}
	    		echo "<script>window.opener.callbackFacebook();window.close();</script>";
	    	} else {
	    		if( ! Session::get('facebook_user')) {
	    			Session::put('facebook_user', [
		    				'user_id'=>$existing_user->id,
		    				'facebook_id'=>$existing_user->facebook_id,
		    				'first_name'=>$existing_user->first_name,
		    				'last_name'=>$existing_user->last_name,
		    				'gender'=>$existing_user->gender,
		    				'email'=>$existing_user->email
		    		]);
	    		}
	    		echo "<script>window.opener.callbackFacebookWithExistingUser();window.close();</script>";
	    	}
	    }
	}

	public function create_fake_session()
	{
		Session::put('facebook_user', [
			'user_id'=>1,
			'facebook_id'=>'123456789',
			'first_name'=>'Jeff',
			'last_name'=>'Mabazza',
			'gender'=>'male',
			'email'=>'jeffrey.mabazza@nuworks.ph'
		]);
	}

	public function logout()
	{
		Session::forget('facebook_user');
	}

}
