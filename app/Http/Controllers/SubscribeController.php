<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Request;
use Redirect;
use Validator;

use App\Subscriber;

class SubscribeController extends Controller {

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Request::input(), [
			'subscribe_firstname'=>'required',
			'subscribe_lastname'=>'required',
			'subscribe_email'=>'required|email'
		]);

		if($validator->fails()) {
			return Redirect::to('/')->with('subscription_error', 'The email you supplied is already in used.');
		}

		$existing = Subscriber::where('email', '=', Request::input('subscribe_email'))->first();

		if($existing) {
			return Redirect::to('/')->with('subscription_error', 'The email you supplied is already existing.');
		}

		$subscriber= new Subscriber;

		$subscriber->first_name = Request::input('subscribe_firstname');
		$subscriber->last_name = Request::input('subscribe_lastname');
		$subscriber->email = Request::input('subscribe_email');

		$subscriber->save();

		return Redirect::to('/')->with('has_thankyou', 'Thank you for subscribing! Check your email everyday for the updates!');
	}

}