<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use View;
use Request;

use App\Photo;
use App\Feed;

class AjaxController extends Controller {

	public function __construct()
	{
		if( ! Request::ajax()) {
			abort(403, 'You are not supposed to go directly on this page!');
		}
	}

	/**
	 * Displays the listing of the resources
	 * @return Response
	 */
	public function photo()
	{
		$photos = Photo::where(function($query) {
			$query->where('status', '=', 1);
			if(Request::input('category'))
				$query->where('category', 'LIKE', '%'.Request::input('category').'%');
		})->paginate(9);

		return View::make('app.ajax.photos')->with(compact('photos'));
	}

	/**
	 * Displays the listing of the resources
	 * @return Response
	 */
	public function feed()
	{
		$feeds = Feed::where('is_deleted', '='. 0)->orderBy('date', 'DESC')->paginate(3);

		return View::make('app.ajax.feeds')->with(compact('feeds'));
	}

}
