<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use View;
use Request;
use DB;
use Session;

use App\Category;
use App\Contest;
use App\Photo;
use App\User;

class PopupController extends Controller {

	public function photo()
	{
		if(Request::ajax()) {
			if( ! Request::input('id') || ! is_numeric(Request::input('id'))) {
				abort(403, 'You have an incorrect url parameter!');
			}

			$photo = DB::table('photos')->select(DB::raw("*, CONCAT(`first_name`, ' ', `last_name`) as full_name"))->join('users', function($query) {
				$query->on('photos.user_id', '=', 'users.id');
				$query->where('photos.id', '=', Request::input('id'));
			})->first();

			// dd($photo);

			if($photo) {
				return View::make('app.popups.view-photo')->with(compact('photo'));
			}

			abort(403, 'Sorry, we couldn\'t find the record you are requesting!');
		}
		abort(403, 'You are not supposed to go directly on this page!');
	}

	public function upload()
	{
		if( ! Session::has('facebook_user')) {
			return $this->login();
		}

		if(Session::has('facebook_user')) {
			$popup  = "<script>";
			$popup .= "$('.mfp-container').trigger('click');";
			$popup .= "window.open('".url('facebook/authenticate')."');";
			$popup .= "</script>";

			echo $popup;
			exit;
		}

		$categories = Category::get();

		return View::make('app.popups.view-upload')->with(compact('categories', 'mechanics'));
	}

	public function upload_with_existing_user()
	{
		if( ! Session::has('facebook_user')) {
			return $this->login();
		}

		$categories = Category::get();

		return View::make('app.popups.view-upload')->with(compact('categories', 'mechanics'));
	}

	public function mechanics()
	{
		$mechanics = Contest::get()->first();

		return View::make('app.popups.view-mechanics')->with(compact('mechanics'));
	}

	public function login()
	{
		return View::make('app.popups.view-login');
	}

	public function terms()
	{
		$terms = Contest::get()->first();

		return View::make('app.popups.view-terms')->with('terms');
	}

	public function thank_you()
	{
		return View::make('app.popups.thank-you');
	}

	public function invalid_submission()
	{
		return View::make('app.popups.invalid-submission');
	}

	public function subscription_error()
	{
		return View::make('app.popups.subscription-error');
	}

}
