<?php namespace App\Http\Middleware;

use Closure;
use Session;

use App\Route;

class RouteMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$route = new Route;

		$route->user_id = Session::get('admin')['id'];
		$route->route = $request->route()->getPath();

		$route->save();
		return $next($request);
	}

}
