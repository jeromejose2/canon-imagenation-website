$("#btn-upload").magnificPopup({type:"ajax",alignTop:true,showCloseBtn:!1,preloader:!0,ajax:{settings:{url:base_url+"popups/upload"}}})
$("#btn-upload-with-existing-user").magnificPopup({type:"ajax",alignTop:true,showCloseBtn:!1,preloader:!0,ajax:{settings:{url:base_url+"popups/upload-with-existing-user"}}})
$("#thank-you").magnificPopup({type:"ajax",alignTop:true,showCloseBtn:!1,preloader:!0,ajax:{settings:{url:base_url+"popups/thank_you"}}})
$("#invalid-submission").magnificPopup({type:"ajax",alignTop:true,closeOnBgClick:false,showCloseBtn:!1,preloader:!0,ajax:{settings:{url:base_url+"popups/invalid_submission"}}})
$("#subscription-error").magnificPopup({type:"ajax",alignTop:true,showCloseBtn:!1,preloader:!0,ajax:{settings:{url:base_url+"popups/subscription_error"}}})

$("#share-facebook").on("click", function() {
	var info = $(".share-data");
	var title = info.data("title");
	var description = info.data("description");
	var image = info.data("image");
	var url = info.data("url");

	var share_facebook = window.open('https://www.facebook.com/sharer/sharer.php?s=100&p[title]='+title+'&p[summary]='+description+'&p[url]='+url+'&p[images][0]='+image, '_blank', 'width=600, height=600');
});

$("#share-twitter").on("click", function() {
	var title = $(".share-data").data('title');
	var bitly_url = $(".share-data").data('twitter-share');
	window.open('http://twitter.com/?status='+encodeURIComponent(title+' '+bitly_url), '_blank', 'width=600,height=300')
});

function submitSubscribeForm() {
	$('#form-subscribe').submit();
}

if(has_thankyou) {
	$("#thank-you").trigger("click");
}

if(invalid_submission) {
	$("#invalid-submission").trigger("click");
}

if(subscription_error) {
	$("#subscription-error").trigger("click");
}

function callbackFacebook() {
	$("#btn-upload").trigger("click");
}

function callbackFacebookWithExistingUser() {
	$("#btn-upload-with-existing-user").trigger("click");
}

$('.scrollup').click(function() {
	if ($('html').scrollTop()) {
        $('html').animate({ scrollTop: 0 }, false);
        return;
    }

    if ($('body').scrollTop()) {
        $('body').animate({ scrollTop: 0 }, false);
        return;
    }
});

function paginatePhotos(url) {
	if(url) {
		var photo_container = $('#photo-ajax-container');
		var category = $('#showcase select').val();
		var ajax_container = $('#photo-ajax-container');
		photo_container.html("<div style=\"height: 80px; text-align: center\"><img src=\""+base_url+"assets/img/preloader.GIF\"></div>");
		$.get(url, {category: category}, function(result) {
			ajax_container.html(result);
		})
	}
}

function paginateFeeds(url) {
	if(url) {
		var ajax_container = $('#feeds-container');
		$.get(url, {}, function(result) {
			ajax_container.html(result);
		});
	}
}

function validateUploadForm() {
	var error = false;
	var error_container = $('.error-message');
	error_container.html('');

	$('.required').each(function() {
		var value = $.trim($(this).val());
		var message = $(this).data('field');
		var name = $(this).attr('name');

		if(name == 'terms_and_conditions') {
			if($(this).is(':checked') == false) {
				error_container.append("<li>You must agree to our "+message+" first!</li>");
				error = true;
				$(this).focus();
				return false;
			}
		}
		if( ! value) {
			if(name == 'files[]') {
				error_container.append("<li>You must "+message+" first!</li>");
				error = true;
				$(this).focus();
				return false;
			} else {
				error_container.append("<li>"+message+" field is required!</li>");
				error = true;
				$(this).focus();
				return false;
			}
		}
	});

	if( ! grecaptcha.getResponse()) {
		error_container.append("<li>Please verify that you are not a robot!</li>");
		error = true;
		return false;
	}

	if(error) {
		return false;
	}
	return true;
}

function validateSubscribeForm() {
	var error = false;
	var email = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	var name = new RegExp("^[a-zA-Z\-\\s]+$");
	var error_container = $('.subscribe-error');

	$('.subscribe-required').each(function() {
		var val = $.trim($(this).val());

		if( ! val) {
			$(this).focus();
			error = true;
			error_container.html('Sorry, this field is required.');
			return false;
		}
		if($(this).attr('name') == 'subscribe_firstname' || $(this).attr('name') == 'subscribe_lastname') {
			if( ! name.test(val)) {
				$(this).focus();
				error = true;
				error_container.html('Sorry, invalid characters supplied in this field.');
				return false;
			}
		}
		if($(this).attr('name') == 'subscribe_email') {
			if( ! email.test(val)) {
				$(this).focus();
				error = true;
				error_container.html('Sorry, you have supplied an invalid email address.');
				return false;
			}
		}
	});

	if(error) {
		return false;
	}

	error_container.html('');
	return true;
}

function getFilenames() {
	var elem = document.getElementById("input-file");
	var filename_container = $('.name-cont');
	var names = [];
	var size = 0;
	var max_limit = 3145728;
	var jpeg = "image/jpeg";
	var jpg = "image/jpg";
	var exceed = false;
	$('.file-size-exceeds').remove();
	$('.file-extension-error').remove();
	for (var i = 0; i < elem.files.length; ++ i) {
		if(elem.files[i].type != jpeg && elem.files[i].type != jpg) {
			$('.error-message').append("<li class=\"file-extension-error\">The file type should only be <strong>JPG</strong>!</li>");
			elem.value = '';
			return false;
		}

		console.log(elem.files[i]);
	   names.push(elem.files[i].name);
	   console.log(elem.files[i].size);
	   size += elem.files[i].size;
	   if(elem.files[i].size > max_limit) {
	   	exceed = true;
	   }
	}

	// if(size > max_limit) {
	if(exceed) {
		// $('.error-message').append("<li class=\"file-size-exceeds\">The file size exceeds the limit allowed</li>")
		$('.error-message').append("<li class=\"file-size-exceeds\">Sorry, one of your photo exceeds the file size limit.</li>")
		elem.value = '';
		filename_container.html('');
	} else {
		filename_container.html(names);
	}
}
