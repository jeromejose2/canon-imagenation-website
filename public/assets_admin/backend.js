setTimeout(function(){
	$(".alert-dismissable").hide();
}, 10000);

function show_content(id) {
	var full_description = $("#description-full-"+id).val();

	$("#description-content-"+id).html(full_description+" <a onclick='hide_content("+id+")'><i class='fa fa-unlink' title='Hide full description'></i></a>");
}

function hide_content(id) {
	var limit_description = $("#description-limit-"+id).html();

	$("#description-content-"+id).html(limit_description);
}

function export_data(url) {
	window.location.href = url;
}

function submitForm() {
	var error = false;
	var custom_error = false;
	var error_container = $(".alert");
	var error_message_container = $(".alert>strong");

	error_container.hide();

	$(".required-field").each(function() {
		var val = $.trim($(this).val());
		var field = $(this).data('field');
		var parent = $(this).parent();
		var name = $(this).attr('name');

		if(parent.hasClass('has-error')) {
			parent.removeClass('has-error');
		}

		if( ! val && (name != 'password' && name != 'confirm_password' && name != 'description' && name != 'mechanics')) {
			error = field;
			parent.addClass('has-error');
			$(this).focus();
			return false;
		}

		if(name == 'description') {
			var description = $('#summernote_1').code();
			if(description === '<p><br></p>') {
				error = field;
				return false;
			}
		}

		if(name == 'mechanics') {
			var mechanics = $('#summernote_2').code();
			if(mechanics === '<p><br></p>') {
				error = field;
				return false;
			}
		}
	});

	if($('input[name=password]').val()) {
		if($('input[name=password]').val() != $('input[name=confirm_password]').val()) {
			error = true;
			custom_error = 'Sorry, your password field doesn\'t match with the confirm password field.';
			$('input[name=password]').focus();
		}
	}

	if(error) {
		if(custom_error) {
			error_message_container.html("<i class=\"fa fa-warning\"></i>"+custom_error);
		} else {
			error_message_container.html("<i class=\"fa fa-warning\"></i> Sorry, the "+error+" field is required.");
		}
		error_container.show();
		$('html, body').animate({ scrollTop: 0 }, 'fast');

		return false;
	}

	return true;
}