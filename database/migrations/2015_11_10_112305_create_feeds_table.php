<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('feeds', function($table) {
		    $table->increments('id');
		    $table->string('photo')->nullable();
		    $table->string('original_filename')->nullable();
		    $table->string('link', 500)->nullable();
		    $table->longtext('description')->nullable();
		    $table->string('title', 100)->nullable();
		    $table->string('crusader', 100)->nullable();
		    $table->string('permalink')->nullable();
		    $table->string('date')->nullable();
		    $table->string('short_url')->nullable();
		    $table->tinyInteger('is_deleted')->default(0);
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('feeds');
	}

}
