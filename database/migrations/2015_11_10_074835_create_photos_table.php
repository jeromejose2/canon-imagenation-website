<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('photos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->nullable();
			$table->text('photo')->nullable();
			$table->string('title', 150)->nullable();
			$table->text('description')->nullable();
			$table->string('camera', 500)->nullable();
			$table->text('settings')->nullable();
			$table->string('category', 500)->nullable();
			$table->tinyInteger('status')->default(0);
			$table->tinyInteger('type')->default(SHARING);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('photos');
	}

}
