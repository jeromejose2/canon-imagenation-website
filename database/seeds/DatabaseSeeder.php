<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Photo;
use App\Feed;
use App\Admin;
use App\Category;
use App\Contest;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		// DB::table('users')->insert([
		// 	['facebook_id'=>'123456789', 'first_name'=>'John', 'last_name'=>'Doe'],
		// 	['facebook_id'=>'123456781', 'first_name'=>'Mark', 'last_name'=>'Straus'],
		// 	['facebook_id'=>'123456782', 'first_name'=>'Albert', 'last_name'=>'Twain'],
		// ]);

		// DB::table('photos')->insert([
		// 	['user_id'=>1, 'type'=>1, 'category'=>'Travel', 'photo'=>'abc.jpg', 'camera'=>'Digital SLR EOS 100D', 'title'=>'Title A', 'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'settings'=>serialize(['settings1'=>'1/1000', 'settings2'=>'2/1000', 'settings3'=>'3/1000'])],
		// 	['user_id'=>2, 'type'=>1, 'category'=>'Street Photography', 'photo'=>'def.jpg', 'camera'=>'Digital SLR EOS 7D Mark II', 'title'=>'Title B', 'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'settings'=>serialize(['settings1'=>'1/1000', 'settings2'=>'2/1000', 'settings3'=>'3/1000'])],
		// 	['user_id'=>3, 'type'=>2, 'category'=>'Landscapes', 'photo'=>'ghi.jpg', 'camera'=>'Digital SLR EOS 1D X', 'title'=>'Title C', 'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'settings'=>serialize(['settings1'=>'1/1000', 'settings2'=>'2/1000', 'settings3'=>'3/1000'])],
		// 	['user_id'=>3, 'type'=>2, 'category'=>'Landscapes', 'photo'=>'ghi.jpg', 'camera'=>'Digital SLR EOS 1D X', 'title'=>'Title C', 'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'settings'=>serialize(['settings1'=>'1/1000', 'settings2'=>'2/1000', 'settings3'=>'3/1000'])],
		// 	['user_id'=>3, 'type'=>2, 'category'=>'Landscapes', 'photo'=>'ghi.jpg', 'camera'=>'Digital SLR EOS 1D X', 'title'=>'Title C', 'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'settings'=>serialize(['settings1'=>'1/1000', 'settings2'=>'2/1000', 'settings3'=>'3/1000'])],
		// 	['user_id'=>3, 'type'=>2, 'category'=>'Landscapes', 'photo'=>'ghi.jpg', 'camera'=>'Digital SLR EOS 1D X', 'title'=>'Title C', 'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'settings'=>serialize(['settings1'=>'1/1000', 'settings2'=>'2/1000', 'settings3'=>'3/1000'])],
		// 	['user_id'=>3, 'type'=>2, 'category'=>'Landscapes', 'photo'=>'ghi.jpg', 'camera'=>'Digital SLR EOS 1D X', 'title'=>'Title C', 'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'settings'=>serialize(['settings1'=>'1/1000', 'settings2'=>'2/1000', 'settings3'=>'3/1000'])],
		// 	['user_id'=>3, 'type'=>2, 'category'=>'Landscapes', 'photo'=>'ghi.jpg', 'camera'=>'Digital SLR EOS 1D X', 'title'=>'Title C', 'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'settings'=>serialize(['settings1'=>'1/1000', 'settings2'=>'2/1000', 'settings3'=>'3/1000'])],
		// 	['user_id'=>3, 'type'=>2, 'category'=>'Landscapes', 'photo'=>'ghi.jpg', 'camera'=>'Digital SLR EOS 1D X', 'title'=>'Title C', 'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'settings'=>serialize(['settings1'=>'1/1000', 'settings2'=>'2/1000', 'settings3'=>'3/1000'])],
		// 	['user_id'=>3, 'type'=>2, 'category'=>'Landscapes', 'photo'=>'ghi.jpg', 'camera'=>'Digital SLR EOS 1D X', 'title'=>'Title C', 'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'settings'=>serialize(['settings1'=>'1/1000', 'settings2'=>'2/1000', 'settings3'=>'3/1000'])]
		// ]);

		DB::table('feeds')->insert([
			['link'=>'http://www.example.com', 'short_url'=>'', 'crusader'=>'Love NuWorks', 'title'=>'Title A', 'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'date'=>'2012-01-02 12:21:16'],
			['link'=>'http://www.example.com', 'short_url'=>'', 'crusader'=>'Love NuWorks', 'title'=>'Title B', 'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'date'=>'2013-02-03 13:22:17'],
			['link'=>'http://www.example.com', 'short_url'=>'', 'crusader'=>'Love NuWorks', 'title'=>'Title C', 'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'date'=>'2014-03-04 14:23:18'],
			['link'=>'http://www.example.com', 'short_url'=>'', 'crusader'=>'Love NuWorks', 'title'=>'Title D', 'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'date'=>'2014-03-04 14:23:18'],
			['link'=>'http://www.example.com', 'short_url'=>'', 'crusader'=>'Love NuWorks', 'title'=>'Title E', 'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'date'=>'2014-03-04 14:23:18'],
			['link'=>'http://www.example.com', 'short_url'=>'', 'crusader'=>'Love NuWorks', 'title'=>'Title F', 'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'date'=>'2014-03-04 14:23:18'],
			['link'=>'http://www.example.com', 'short_url'=>'', 'crusader'=>'Love NuWorks', 'title'=>'Title G', 'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'date'=>'2014-03-04 14:23:18'],
			['link'=>'http://www.example.com', 'short_url'=>'', 'crusader'=>'Love NuWorks', 'title'=>'Title H', 'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'date'=>'2014-03-04 14:23:18'],
			['link'=>'http://www.example.com', 'short_url'=>'', 'crusader'=>'Love NuWorks', 'title'=>'Title I', 'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'date'=>'2014-03-04 14:23:18'],
		]);;

		DB::table('admins')->insert([
			['username'=>'CMS User', 'email'=>'user@nuworks.ph', 'password'=>'$2y$10$XIaT9f1mtPCmor/TgVbW9.Ol9BxqPbzsp8m9z7W5Jg.RWqdv9OO6u']
		]);

		DB::table('categories')->insert([
			['content'=>'Animals'],
			['content'=>'Nature'],
			['content'=>'Landscapes'],
			['content'=>'People and Culture'],
			['content'=>'Portraits'],
			['content'=>'Travel'],
			['content'=>'Black and White'],
			['content'=>'Architecture'],
			['content'=>'Street Photography'],
			['content'=>'Macro']
		]);

		DB::table('contests')->insert([
			'title'=>'Contest title here'
		]);
	}

}
