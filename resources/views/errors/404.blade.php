@extends('app.layouts.master')

@section('content')
<!-- <section> -->
    <div class="banner"></div>

    <div id="subscribe" class="cf" style="height: 250px !important">
        <h1>Sorry, page not found! Redirecting in <span class="seconds">5</span> seconds.</h1>
    </div>

<!-- </section> -->
@endsection

@section('scripts');
  <script type="text/javascript">
      $(document).ready(function() {
          setInterval(function() {
              var seconds = $('.seconds').html();
              if(seconds == 0) {
                  window.location.href = '{{ url() }}';
                  return false;
              }
              $('.seconds').html(seconds - 1);
          }, 1000);
      });
  </script>
@endsection