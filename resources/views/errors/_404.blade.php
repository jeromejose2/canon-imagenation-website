@extends('admin.layouts.layout')

@section('styles')
<link href="{{ asset('assets_admin/admin/pages/css/error.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<!-- BEGIN PAGE CONTENT-->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12 page-404">
				<div class="number">
					 404
				</div>
				<div class="details">
					<h3>Oops! You're lost.</h3>
					<p>
						 We can not find the page you're looking for.<br/>
						 The page will automatically redirect to home page.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT-->
@endsection

@section('scripts')
<script type="text/javascript">
	setTimeout(function(){
		window.location.href = "{{ url('admin/dashboard') }}";
	}, 10000);
</script>
@endsection