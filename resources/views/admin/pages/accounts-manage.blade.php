@extends('admin.layouts.layout')

@section('styles')
@endsection

@section('content')
<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-danger alert-dismissable" style="display: none">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
						<strong><i class="fa fa-warning"></i></strong>
					</div>
					<div class="portlet box custom-color">
						<div class="portlet-title">
							<div class="caption">
								Account Settings
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" method="POST" action="{{ url('admin/account/update') }}" onsubmit="return submitForm()">
								<input type="hidden" value="{{ csrf_token() }}" name="_token">
								<input type="hidden" value="{{ $record->id }}" name="id">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label">Name</label>
										<input type="text" class="form-control required-field" data-field="Username" name="username" value="{{ $record->username ?: '' }}">
									</div>
									<div class="form-group">
										<label class="control-label">Email</label>
										<input type="text" class="form-control required-field" data-field="Email" name="email" value="{{ $record->email ?: '' }}">
									</div>
									<div class="form-group">
										<label class="control-label">Password <small>(Do not fill this field unless you wish to change it)</small></label>
										<input type="password" class="form-control required-field" data-field="Password" name="password">
									</div>
									<div class="form-group">
										<label class="control-label">Confirm Password</label>
										<input type="password" class="form-control required-field" data-field="Confirm Password" name="confirm_password">
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn blue">Submit</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
@endsection

@section('scripts')
@endsection