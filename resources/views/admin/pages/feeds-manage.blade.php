@extends('admin.layouts.layout')

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets_admin/global/plugins/bootstrap-summernote/summernote.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets_admin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets_admin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets_admin/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
@endsection

@section('content')
<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-danger alert-dismissable" style="display: none">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
						<strong><i class="fa fa-warning"></i></strong>
					</div>
					<div class="portlet box custom-color">
						<div class="portlet-title">
							<div class="caption">
								{{ ucwords($method) }}
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" method="POST" action="{{ isset($record) ? url('admin/feeds/update') : url('admin/feeds/store') }}" enctype="multipart/form-data" onsubmit="return submitForm()">
								<input type="hidden" value="{{ csrf_token() }}" name="_token">
								@if(isset($record))
									<input type="hidden" value="{{ $record->id }}" name="id">
								@endif
								<div class="form-body">
									<div class="form-group">
										<input type="file" id="input-photo" class="{{ isset($record) && $record->photo ?: 'required-field' }}" data-field="Photo" name="photo" style="display: none" onchange="javascript:ValidateSingleInput(this)">
										<label class="control-label">Photo</label> <br>
										<a href="javascript:void(0)" class="btn btn-sm" onclick="$('#input-photo').trigger('click')">Browse</a> <span id="filename">{{ isset($record) && $record->photo ? $record->original_filename : '' }}</span>
									</div>
									<div class="form-group" style="display: none">
										<label class="control-label">Article Link</label>
										<input type="text" class="form-control" data-field="Article Link" name="link" value="{{ isset($record) ? $record->link : '' }}">
									</div>
									<div class="form-group">
										<label class="control-label">Author</label>
										<input type="text" class="form-control required-field" data-field="Name" name="crusader" value="{{ isset($record) ? $record->crusader : '' }}">
									</div>
									<div class="form-group">
										<label class="control-label">Title</label>
										<input type="text" class="form-control required-field" data-field="Title" name="title" value="{{ isset($record) ? $record->title : '' }}">
									</div>
									<div class="form-group">
										<label class="control-label">Date</label>
										<input type="text" class="form-control date-picker required-field" data-field="Date" name="date" value="{{ isset($record) ? date('m/d/Y', strtotime($record->date)) : '' }}" readonly>
									</div>
									<div class="form-group">
										<label class="control-label">Description</label>
										<textarea id="summernote_1" name="description" class="required-field" data-field="Description">{{ isset($record) ? $record->description : '' }}</textarea>
									</div>
								</div>
								<div class="form-actions">
									<a href="{{ url('admin/feeds') }}" class="btn default">Cancel</a>
									<button type="submit" class="btn blue">Submit</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
@endsection

@section('scripts')
<script src="{{ asset('assets_admin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets_admin/global/plugins/bootstrap-daterangepicker/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets_admin/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
	var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];
	function ValidateSingleInput(oInput) {
	    if (oInput.type == "file") {
	        var sFileName = oInput.value;
	         if (sFileName.length > 0) {
	            var blnValid = false;
	            for (var j = 0; j < _validFileExtensions.length; j++) {
	                var sCurExtension = _validFileExtensions[j];
	                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
	                    blnValid = true;
	                    break;
	                }
	            }

	            if (!blnValid) {
	            	$('#invalid-file-type').modal('show');
	                return false;
	            }
	        }
	    }
	    var fullPath = document.getElementById('input-photo').value;
		if (fullPath) {
			var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
			var filename = fullPath.substring(startIndex);
			if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
				filename = filename.substring(1);
			}
			$('#filename').html(filename);
		}
	    return true;
	}
</script>
@endsection