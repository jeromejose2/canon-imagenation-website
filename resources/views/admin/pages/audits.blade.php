@extends('admin.layouts.layout')

@section('content')
<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					@if(Session::get('error'))
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
							<strong><i class="fa fa-warning"></i></strong> {{ Session::get('error') }}
						</div>
					@endif
					<div class="portlet box blue">
						<div class="portlet-title custom-color">
							<div class="caption">
								Audits <span class="badge badge-default custom-badge">{{ $audits->total() }}</span>
							</div>
							<div class="btn-group pull-right">
								<button style="margin-top: 6% !important" type="button" class="btn btn-fit-height grey-salt dropdown-toggle btn-sm custom-blue" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false">
								Options <i class="fa fa-angle-down"></i>
								</button>
								<ul class="dropdown-menu pull-right" role="menu">
									<li>
										<a href="{{ url('admin/logs/export') }}">Export</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-scrollable">
								<table class="table table-bordered table-hover">
								<thead>
								<tr>
									<th>
										 #
									</th>
									<th>
										 User ID
									</th>
									<th>
										 Name
									</th>
									<th>
										 Description
									</th>
									<th>
										 Date
									</th>
									{{-- <th width="1%">
										Action
									</th> --}}
								</tr>
								</thead>
								<tbody>
								<tr>
									<form>
										<td></td>
										<td></td>
										<td>
											<input type="text" name="title" class="form-control input-medium input-sm" value="{{ Request::input('title') }}">
										</td>
										<td></td>
										<td width="120px"></td>
										{{-- <td>
											<button type="submit" class="btn btn-sm blue">SEARCH</button>
										</td> --}}
									</form>
								</tr>
								@forelse($audits as $k => $v)
									<tr>
										<td>{{ $num }}</td>
										<td>{{ $v->user_id }}</td>
										<td>{{ $v->username }}</td>
										<td><?php echo str_replace("{{ name }}", "", $v->activity) ?></td>
										<td>{{ date('M j, Y H:i:s', strtotime($v->audit_created_at)) }}</td>
									</tr>
									<?php $num++; ?>
								@empty
									<tr><td colspan="15"><center>No Result Found</center></td></tr>
								@endforelse
								</tbody>
								</table>
							</div>
						{!! $audits->appends($fragments)->render() !!}
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
@endsection