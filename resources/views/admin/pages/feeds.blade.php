@extends('admin.layouts.layout')

@section('content')
<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					@if(Session::get('error'))
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
							<strong><i class="fa fa-warning"></i></strong> {{ Session::get('error') }}
						</div>
					@endif
					<div class="portlet box blue">
						<div class="portlet-title custom-color">
							<div class="caption">
								Articles <span class="badge badge-default custom-badge">{{ $feeds->total() }}</span>
							</div>
							<div class="btn-group pull-right">
								<button style="margin-top: 6% !important" type="button" class="btn btn-fit-height grey-salt dropdown-toggle btn-sm custom-blue" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false">
								Options <i class="fa fa-angle-down"></i>
								</button>
								<ul class="dropdown-menu pull-right" role="menu">
									<li>
										<a href="{{ url('admin/feeds/create') }}">Create</a>
									</li>
									<li>
										<a href="{{ url('admin/feeds/export?').http_build_query(Request::except('page'), '', '&') }}">Export</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-scrollable">
								<table class="table table-bordered table-hover">
								<thead>
								<tr>
									<th>
										 #
									</th>
									<th>
										 Title
									</th>
									<th>
										 Author
									</th>
									<th width="50%">
										 Description
									</th>
									<th>
										 Date
									</th>
									<th width="1%">
										Action
									</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<form>
										<td></td>
										<td>
											<input type="text" name="title" class="form-control input-medium input-sm" value="{{ Request::input('title') }}">
										</td>
										<td>
											<input type="text" name="author" class="form-control input-medium input-sm" value="{{ Request::input('author') }}">
										</td>
										<td></td>
										<td width="120px"></td>
										<td>
											<button type="submit" class="btn btn-sm blue">SEARCH</button>
										</td>
									</form>
								</tr>
								@forelse($feeds as $k => $v)
									<tr>
										<td>{{ $num }}</td>
										<td>{{ $v['title'] }}</td>
										<td>{{ $v['crusader'] }}</td>
										<td id="description-content-{{ $num }}">{!! str_limit($v['description'], 120, "... <a onclick=\"show_content(".$num.")\"><i class=\"fa fa-link\" title=\"Show full description\"></i></a>") !!}</td>
										<td>{{ date('M j, Y', strtotime($v['date'])) }}</td>
										<td>
											<a href="#edit-{{ $num }}" data-toggle="modal" class="btn btn-icon-only btn-primary" title="Edit">
												<i class="fa fa-edit"></i>
											</a>
											<a href="#delete-{{ $num }}" data-toggle="modal" class="btn btn-icon-only btn-danger" title="Delete">
												<i class="fa fa-trash-o"></i>
											</a>
										</td>
									</tr>
									<span id="description-limit-{{ $num }}" class="custom-hide">{!! str_limit(strip_tags($v['description']), 120, "<a onclick=\"show_content(".$num.")\">... <i class=\"fa fa-link\"></i></a>") !!}</span>
									<input id="description-full-{{ $num }}" type="hidden" value="{{ $v['description'] }}">
									<!-- MODAL -->
									<div id="edit-{{ $num }}" class="modal fade" tabindex="-1" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
													<h4 class="modal-title">Confirm</h4>
												</div>
												<div class="modal-body">
													 Are you sure you want to proceed?
												</div>
												<div class="modal-footer">
													<button type="button" class="btn default" data-dismiss="modal">Close</button>
													<a href="{{ url('admin/feeds/edit').'/'.$v->id }}" type="button" class="btn blue">Yes</a>
												</div>
											</div>
											<!-- /.modal-content -->
										</div>
									</div>
									<div id="delete-{{ $num }}" class="modal fade" tabindex="-1" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
													<h4 class="modal-title">Confirm</h4>
												</div>
												<div class="modal-body">
													 Are you sure you want to proceed?
												</div>
												<div class="modal-footer">
													<button type="button" class="btn default" data-dismiss="modal">Close</button>
													<a href="{{ url('admin/feeds/destroy').'/'.$v->id }}" type="button" class="btn blue">Yes</a>
												</div>
											</div>
											<!-- /.modal-content -->
										</div>
									</div>
									<!-- MODAL -->
									<?php $num++; ?>
								@empty
									<tr><td colspan="15"><center>No Result Found</center></td></tr>
								@endforelse
								</tbody>
								</table>
							</div>
						{!! $feeds->appends($fragments)->render() !!}
						<!-- MODAL -->
						<div id="create-modal" class="modal fade" tabindex="-1" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h4 class="modal-title">Confirm</h4>
									</div>
									<div class="modal-body">
										 Are you sure you want to proceed?
									</div>
									<div class="modal-footer">
										<button type="button" class="btn default" data-dismiss="modal">Close</button>
										<a href="{{ url('admin/feeds/create') }}" type="button" class="btn blue">Yes</a>
									</div>
								</div>
								<!-- /.modal-content -->
							</div>
						</div>
						<!-- MODAL -->
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
@endsection