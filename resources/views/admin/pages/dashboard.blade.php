@extends('admin.layouts.layout')

@section('content')
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="col-md-12">
			<div class="note note-success custom-color">
				<h4 class="block">Welcome, {{ Session::get('admin')['username'] }}!</h4>
			</div>
		</div>
		<div class="col-md-12">
			<h4>
				<div class="caption">
		            <i class="icon-bar-chart custom-font"></i>
		            <span class="caption-subject custom-font bold uppercase">Photos</span>
		            <span class="caption-helper"><small>Entries for the year of </small></span>
                    <select name="filter_graph" class="input-sm select2me" style="z-index: 999999 !important; position: relative">
                       <option>2015</option>
                       <option>2014</option>
                   </select>
                   <img src="{{ asset('assets_admin/admin/layout/img/loading.gif') }}" id="preloader" style="display: none">
		        </div>
	        </h4>
			<div id="site_statistics" class="" style="height: 400px"></div>
		</div>
		<div class="col-md-6" style="float: none !important;">
			<h4>
				<div class="caption">
		            <i class="icon-bar-chart custom-font"></i>
		            <span class="caption-subject custom-font bold uppercase">Recent Activities</span>
		        </div>
	        </h4>

				<div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
					<ul class="feeds">
						@forelse($activities as $activity)
							<li>
								<div class="col1">
									<div class="cont">
										<div class="cont-col1">
												@if($activity->method == 'login')
													<div class="label label-sm label-info">
														<i class="fa fa-user"></i>
													</div>
												@elseif($activity->method == 'create')
													<div class="label label-sm label-success">
														<i class="fa fa-plus"></i>
													</div>
												@elseif($activity->method == 'update')
													<div class="label label-sm label-warning">
														<i class="fa fa-edit"></i>
													</div>
												@elseif($activity->method == 'delete')
													<div class="label label-sm label-danger">
														<i class="fa fa-trash-o"></i>
													</div>
												@endif
										</div>
										<div class="cont-col2">
											<div class="desc">
												 <?php echo str_replace("{{ name }}", $activity->username, $activity->activity) ?>
												</span>
											</div>
										</div>
									</div>
								</div>
							</li>
						@empty
							<li>No recent activity</li>
						@endforelse
					</ul>
				</div>
				<div class="scroller-footer">
					<div class="btn-arrow-link pull-right">
						<a href="{{ url('admin/logs') }}">See All Records</a>
						<i class="icon-arrow-right"></i>
					</div>
				</div>


		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('assets_admin/global/plugins/flot/jquery.flot.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets_admin/global/plugins/flot/jquery.flot.resize.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets_admin/global/plugins/flot/jquery.flot.categories.min.js') }}"></script>
<script type="text/javascript">
var visitors = [
				@forelse($data as $graph)
					['{{ $graph['month'] }}', {{ $graph['count'] }}],
				@empty
					[]
				@endforelse
            ];
if ($('#site_statistics').size() != 0) {

	function showChartTooltip(x, y, xValue, yValue) {
        $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
            position: 'absolute',
            display: 'none',
            top: y - 40,
            left: x - 40,
            border: '0px solid #ccc',
            padding: '2px 6px',
            'background-color': '#fff'
        }).appendTo("body").fadeIn(200);
    }

    $('#site_statistics_loading').hide();
    $('#site_statistics_content').show();

    var plot_statistics = $.plot($("#site_statistics"),
        [{
            data: visitors,
            lines: {
                fill: 0.6,
                lineWidth: 0
            },
            color: ['#2b3643']
        }, {
            data: visitors,
            points: {
                show: true,
                fill: true,
                radius: 5,
                fillColor: "#2b3643",
                lineWidth: 3
            },
            color: '#00FF2A',
            shadowSize: 0
        }],

        {
            xaxis: {
                tickLength: 0,
                tickDecimals: 0,
                mode: "categories",
                min: 0,
                font: {
                    lineHeight: 14,
                    style: "normal",
                    variant: "small-caps",
                    color: "#2b3643"
                }
            },
            yaxis: {
                ticks: 5,
                tickDecimals: 0,
                tickColor: "#eee",
                font: {
                    lineHeight: 14,
                    style: "normal",
                    variant: "small-caps",
                    color: "#2b3643"
                }
            },
            grid: {
                hoverable: true,
                clickable: true,
                tickColor: "#eee",
                borderColor: "#eee",
                borderWidth: 1
            }
        });

    var previousPoint = null;
    $("#site_statistics").bind("plothover", function (event, pos, item) {
        $("#x").text(pos.x.toFixed(2));
        $("#y").text(pos.y.toFixed(2));
        if (item) {
            if (previousPoint != item.dataIndex) {
                previousPoint = item.dataIndex;

                $("#tooltip").remove();
                var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);

                var suffix = item.datapoint[1] > 1 ? ' entries' : ' entry';
                showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + suffix);
            }
        } else {
            $("#tooltip").remove();
            previousPoint = null;
        }
    });
}

$("select[name=filter_graph]").on("change", function() {
    var preloader = $("#preloader");
    var elem = $(this);
    var container = $("#site_statistics");
    preloader.show();
    container.css("opacity", "0.5");
    elem.attr('disabled', true);
    $.get("{{ url('admin/graph') }}/"+$(this).val(), function(result) {
        preloader.hide();
        elem.removeAttr('disabled');
        container.css("opacity", "1");
        $("#site_statistics").html(result);
    });
});
</script>
@endsection