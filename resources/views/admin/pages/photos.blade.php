@extends('admin.layouts.layout')

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets_admin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets_admin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets_admin/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}"/>
@endsection

@section('content')
<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					@if(Session::get('error'))
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
							<strong><i class="fa fa-warning"></i></strong> {{ Session::get('error') }}
						</div>
					@endif
					<div class="portlet box blue">
						<div class="portlet-title custom-color">
							<div class="caption">
								Photos <span class="badge badge-default custom-badge">{{ $photos->total() }}</span>
							</div>
							<div class="btn-group pull-right">
								<button style="margin-top: 6% !important" type="button" class="btn btn-fit-height grey-salt dropdown-toggle btn-sm custom-blue" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false">
								Options <i class="fa fa-angle-down"></i>
								</button>
								<ul class="dropdown-menu pull-right" role="menu">
									<li>
										<a onclick="export_data('{{ url('admin/user_photos/export?').http_build_query(Request::except('page'), '', '&') }}')" href="javascript:void(0)">Export</a>
									</li>
								</ul>
							</div>
							{{-- <div class="tools">
							<button class="btn btn-sm blue" title="Export" onclick="export_data('{{ url('admin/user_photos/export?').http_build_query(Request::except('page'), '', '&') }}')">EXPORT</button>
							</div> --}}
						</div>
						<div class="portlet-body">
							<div class="table-scrollable">
								<table class="table table-bordered table-hover">
								<thead>
								<tr>
									<th>
										 #
									</th>
									<th>
										Name
									</th>
									<th>
										 Photo
									</th>
									<th>
										 Title
									</th>
									<th>
										Camera
									</th>
									<th>
										Settings
									</th>
									<th>
										Category
									</th>
									<th>
										 Description
									</th>
									<th>
										Entry
									</th>
									<th>
										Status
									</th>
									<th>
										 Date Published
									</th>
									<th>
										Action
									</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<form>
										<td></td>
										<td>
											<input type="text" name="name" class="form-control input-small input-sm" value="{{ Request::input('name') }}">
										</td>
										<td></td>
										<td>
											<input type="text" name="title" class="form-control input-small input-sm" value="{{ Request::input('title') }}">
										</td>
										<td>
											<input type="text" name="camera" class="form-control input-small input-sm" value="{{ Request::input('camera') }}">
										</td>
										<td width="10%"></td>
										<td>
											<select class="form-control input-small input-sm" name="category">
												<option value=""></option>
												@if(isset($categories))
													@foreach($categories as $category)
														<option value="{{ $category->content }}" {{ Request::input('category') == $category->content ? 'selected' : '' }}>{{ $category->content }}</option>
													@endforeach
												@endif
											</select>
										</td>
										<td>
											<input type="text" name="description" class="form-control input-sm" value="{{ Request::input('description') }}">
										</td>
										<td>
											<select class="form-control input-sm input-xsmall" name="type">
												<option value=""></option>
												<option value="{{ ENTRY }}" {{ Request::input('type') && Request::input('type') == ENTRY ? 'selected' : '' }}>Yes</option>
												<option value="{{ SHARING }}" {{ Request::input('type') && Request::input('type') == SHARING ? 'selected' : '' }}>No</option>
											</select>
										</td>
										<td></td>
										<td>
											<div class="input-group input-group-sm input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy" style="width: 250px !important">
												<input id="datepicker" type="text" class="form-control" name="from" value="{{ Request::input('from') }}" readonly>
												<span class="input-group-addon">
												to </span>
												<input type="text" class="form-control" name="to" value="{{ Request::input('to') }}" readonly>
												<span class="input-group-btn">
													<button class="btn default date-set" type="button" onclick="javascript:clearDates()"><i class="fa fa-ban"></i></button>
												</span>
											</div>

										</td>
										<td width="1%">
											<button type="submit" class="btn btn-sm blue">SEARCH</button>
										</td>
									</form>
								</tr>
								@forelse($photos as $k => $v)
									<tr>
										<td class="highlight"><div class="success"></div>{{ $num }}</td>
										<td>{{ $v->name }}</td>
										<td><a href="#preview-{{ $num }}" data-toggle="modal"><img src="{{asset('uploads') .'/thumb_'. $v->photo}}" alt="{{ $v->first_name.' '.$v->last_name }}" height="50"></a></td>
										<td>{{ $v->title }}</td>
										<td>{{ $v->camera }}</td>
										<td>
											@if($v->settings)
												@if(is_array($settings = unserialize($v->settings)))
													@foreach($settings as $ksetting => $vsetting)
														{{ $ksetting }} : {{ $vsetting }} <br>
													@endforeach
												@endif
											@endif
										</td>
										<td>{{ $v->category }}</td>
										<td id="description-content-{{ $num }}">{!! str_limit($v->description, 120, "... <a onclick=\"show_content(".$num.")\"><i class=\"fa fa-link\" title=\"Show full description\"></i></a>") !!}</td>
										<td>
											@if($v->type == ENTRY)
											<span class="label label-primary">Yes</span>
											@else
											<span class="label label-danger">No</span>
											@endif
										</td>
										<td>
											@if($v->status == 0)
											<span class="label label-primary">Pending</span>
											@elseif($v->status == 1)
											<span class="label label-success">Approved</span>
											@else
											<span class="label label-danger">Rejected</span>
											@endif
										</td>
										<td>{{ date('F j, Y', strtotime($v->date_published)) }}</td>
										<td>
											<a href="#approval-{{ $num }}" data-toggle="modal" class="btn btn-icon-only blue btn-small" title="Approve" {{ $v->status == 1 ? 'disabled' : '' }}>
												<i class="icon-like"></i>
											</a>
											<a href="#disapproval-{{ $num }}" data-toggle="modal" class="btn btn-icon-only red btn-small" title="Disapprove" {{ $v->status == 2 ? 'disabled' : '' }}>
												<i class="icon-dislike"></i>
											</a>
											{{-- <a href="#approval-{{ $num }}" data-toggle="modal" class="btn blue btn-sm" {{ $v->status == 0 ?: 'disabled' }}>Approve</a> --}}
										</td>
									</tr>
									<span id="description-limit-{{ $num }}" class="custom-hide">{!! str_limit($v->description, 120, "<a onclick=\"show_content(".$num.")\">... <i class=\"fa fa-link\"></i></a>") !!}</span>
									<input id="description-full-{{ $num }}" type="hidden" value="{{ $v->description }}">
									<!-- BEGIN PREVIEW MODAL -->
									<div id="preview-{{ $num }}" class="modal bs-modal-lg fade" tabindex="-1" aria-hidden="true">
										<div class="modal-dialog modal-lg">
											<div class="modal-content">
												<div class="modal-body">
													{{-- <div class="scroller" style="height:300px" data-always-visible="1" data-rail-visible1="1"> --}}
														<center>
															<img src="{{asset('uploads') .'/cms_'. $v->photo}}" alt="{{ $v->first_name.' '.$v->last_name }}">
														</center>
													{{-- </div> --}}
												</div>
												<div class="modal-footer">
													<button type="button" data-dismiss="modal" class="btn green">Close</button>
												</div>
											</div>
										</div>
									</div>
									<div id="approval-{{ $num }}" class="modal fade" tabindex="-1" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
													<h4 class="modal-title">Confirm</h4>
												</div>
												<div class="modal-body">
													 Are you sure you want to proceed?
												</div>
												<div class="modal-footer">
													<button type="button" class="btn default" data-dismiss="modal">Close</button>
													<a href="{{ url('admin/user_photos/approve').'/'.$v->photo_id }}" type="button" class="btn blue">Yes</a>
												</div>
											</div>
											<!-- /.modal-content -->
										</div>
									</div>
									<div id="disapproval-{{ $num }}" class="modal fade" tabindex="-1" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
													<h4 class="modal-title">Confirm</h4>
												</div>
												<div class="modal-body">
													 Are you sure you want to proceed?
												</div>
												<div class="modal-footer">
													<button type="button" class="btn default" data-dismiss="modal">Close</button>
													<a href="{{ url('admin/user_photos/disapprove').'/'.$v->photo_id }}" type="button" class="btn blue">Yes</a>
												</div>
											</div>
											<!-- /.modal-content -->
										</div>
									</div>
									<!-- END PREVIEW MODAL -->
									<?php $num++; ?>
								@empty
									<tr><td colspan="15"><center>No Result Found</center></td></tr>
								@endforelse
								</tbody>
								</table>
							</div>
						{!! $photos->appends($fragments)->render() !!}
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('assets_admin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets_admin/global/plugins/bootstrap-daterangepicker/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets_admin/global/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('assets_admin/admin/pages/scripts/components-pickers.js') }}"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	ComponentsPickers.init();
});
</script>

<script type="text/javascript">
	function clearDates() {
		$(".input-daterange input").val('');
	}
</script>
@endsection