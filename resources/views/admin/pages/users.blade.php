@extends('admin.layouts.layout')

@section('content')
<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title custom-color">
							<div class="caption">
								Users <span class="badge badge-default custom-badge">{{ $users->total() }}</span>
							</div>
							<div class="btn-group pull-right">
								<button style="margin-top: 6% !important" type="button" class="btn btn-fit-height grey-salt dropdown-toggle btn-sm custom-blue" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false">
								Options <i class="fa fa-angle-down"></i>
								</button>
								<ul class="dropdown-menu pull-right" role="menu">
									<li>
										<a onclick="export_data('{{ url('admin/users/export?').http_build_query(Request::except('page'), '', '&') }}')" href="javascript:void(0)">Export</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-scrollable">
								<table class="table table-bordered table-hover">
								<thead>
								<tr>
									<th>
										 #
									</th>
									<th>
										 First Name
									</th>
									<th>
										 Last Name
									</th>
									<th>
										 Facebook ID
									</th>
									<th>
										 Has uploaded
									</th>
									<th>
										Timestamp
									</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<form>
										<td></td>
										<td>
											<input type="text" name="first_name" class="form-control input-medium input-sm" value="{{ Request::input('first_name') }}">
										</td>
										<td>
											<input type="text" name="last_name" class="form-control input-medium input-sm" value="{{ Request::input('last_name') }}">
										</td>
										<td>
											<input type="text" name="facebook_id" class="form-control input-medium input-sm" value="{{ Request::input('facebook_id') }}">
										</td>
										<td width="1%"></td>
										<td width="1%">
											<button type="submit" class="btn btn-sm blue">SEARCH</button>
										</td>
									</form>
								</tr>
								@forelse($users as $k => $v)
									<tr>
										<td>{{ $num }}</td>
										<td>{{ $v['first_name'] }}</td>
										<td>{{ $v['last_name'] }}</td>
										<td>{{ $v['facebook_id'] }}</td>
										<td>
											@if(@$v['has_photo'])
												{{-- <span class="label label-primary">Yes</span> --}}
												<a href="{{ url('admin/user_photos?user_id=').$v['id'] }}" target="_blank"><span class="label label-primary">View</span></a>
											@else
												<span class="label label-danger">No</span>
											@endif
										</td>
										<td>{{ date('M j, Y', strtotime($v['created_at'])) }}</td>
									</tr>
									<?php $num++; ?>
								@empty
									<tr><td colspan="15"><center>No Result Found</center></td></tr>
								@endforelse
								</tbody>
								</table>
							</div>
						{!! $users->appends($fragments)->render() !!}
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
@endsection