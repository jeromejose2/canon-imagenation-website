@extends('admin.layouts.layout')

@section('content')
<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					@if(Session::get('error'))
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
							<strong><i class="fa fa-warning"></i></strong> {{ Session::get('error') }}
						</div>
					@endif
					<div class="portlet box blue">
						<div class="portlet-title custom-color">
							<div class="caption">
								Subscribers <span class="badge badge-default custom-badge">{{ $subscribers->total() }}</span>
							</div>
							<div class="btn-group pull-right">
								<button style="margin-top: 6% !important" type="button" class="btn btn-fit-height grey-salt dropdown-toggle btn-sm custom-blue" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false">
								Options <i class="fa fa-angle-down"></i>
								</button>
								<ul class="dropdown-menu pull-right" role="menu">
									<li>
										<a href="{{ url('admin/subscribers/export') }}">Export</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-scrollable">
								<table class="table table-bordered table-hover">
								<thead>
								<tr>
									<th>
										 #
									</th>
									<th>
										 First Name
									</th>
									<th>
										 Last Name
									</th>
									<th>
										 Email Address
									</th>
									<th>
										 Date Created
									</th>
								</tr>
								</thead>
								<tbody>
								@forelse($subscribers as $k => $v)
									<tr>
										<td>{{ $num }}</td>
										<td>{{ $v->first_name }}</td>
										<td>{{ $v->last_name }}</td>
										<td>{{ $v->email }}</td>
										<td>{{ date('F j, Y', strtotime($v->created_at)) }}</td>
									</tr>
									<?php $num++; ?>
								@empty
									<tr><td colspan="15"><center>No Result Found</center></td></tr>
								@endforelse
								</tbody>
								</table>
							</div>
						{!! $subscribers->appends($fragments)->render() !!}
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
@endsection