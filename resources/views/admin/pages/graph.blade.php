<div id="site_statistics" class="" style="height: 400px"></div>

<script type="text/javascript" src="{{ asset('assets_admin/global/plugins/flot/jquery.flot.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets_admin/global/plugins/flot/jquery.flot.resize.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets_admin/global/plugins/flot/jquery.flot.categories.min.js') }}"></script>
<script type="text/javascript">
var visitors = [
				@forelse($data as $graph)
					['{{ $graph['month'] }}', {{ $graph['count'] }}],
				@empty
					[]
				@endforelse
            ];
if ($('#site_statistics').size() != 0) {

	function showChartTooltip(x, y, xValue, yValue) {
        $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
            position: 'absolute',
            display: 'none',
            top: y - 40,
            left: x - 40,
            border: '0px solid #ccc',
            padding: '2px 6px',
            'background-color': '#fff'
        }).appendTo("body").fadeIn(200);
    }

    $('#site_statistics_loading').hide();
    $('#site_statistics_content').show();

    var plot_statistics = $.plot($("#site_statistics"),
        [{
            data: visitors,
            lines: {
                fill: 0.6,
                lineWidth: 0
            },
            color: ['#2b3643']
        }, {
            data: visitors,
            points: {
                show: true,
                fill: true,
                radius: 5,
                fillColor: "#2b3643",
                lineWidth: 3
            },
            color: '#00FF2A',
            shadowSize: 0
        }],

        {
            xaxis: {
                tickLength: 0,
                tickDecimals: 0,
                mode: "categories",
                min: 0,
                font: {
                    lineHeight: 14,
                    style: "normal",
                    variant: "small-caps",
                    color: "#2b3643"
                }
            },
            yaxis: {
                ticks: 5,
                tickDecimals: 0,
                tickColor: "#eee",
                font: {
                    lineHeight: 14,
                    style: "normal",
                    variant: "small-caps",
                    color: "#2b3643"
                }
            },
            grid: {
                hoverable: true,
                clickable: true,
                tickColor: "#eee",
                borderColor: "#eee",
                borderWidth: 1
            }
        });

    var previousPoint = null;
    $("#site_statistics").bind("plothover", function (event, pos, item) {
        $("#x").text(pos.x.toFixed(2));
        $("#y").text(pos.y.toFixed(2));
        if (item) {
            if (previousPoint != item.dataIndex) {
                previousPoint = item.dataIndex;

                $("#tooltip").remove();
                var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);

                var suffix = item.datapoint[1] > 1 ? ' entries' : ' entry';
                showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + suffix);
            }
        } else {
            $("#tooltip").remove();
            previousPoint = null;
        }
    });
}

$("select[name=filter_graph]").on("change", function() {
    $.get();
});
</script>