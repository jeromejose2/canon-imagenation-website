@extends('admin.layouts.layout')

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets_admin/global/plugins/bootstrap-summernote/summernote.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets_admin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets_admin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets_admin/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
@endsection

@section('content')
<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-danger alert-dismissable" style="display: none">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
						<strong><i class="fa fa-warning"></i></strong>
					</div>
					<div class="portlet box custom-color">
						<div class="portlet-title">
							<div class="caption">
								Contest
							</div>
							{{-- <div class="btn-group pull-right">
								<button style="margin-top: 6% !important" type="button" class="btn btn-fit-height grey-salt dropdown-toggle btn-sm custom-blue" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false">
								Options <i class="fa fa-angle-down"></i>
								</button>
								<ul class="dropdown-menu pull-right" role="menu">
									<li>
										<a href="#previous-contests" data-toggle="modal">Previous Contests</a>
									</li>
								</ul>
							</div> --}}
						</div>
						<div class="portlet-body form">
							<form role="form" method="POST" action="{{ url('admin/contest/update') }}" onsubmit="return submitForm()" enctype="multipart/form-data">
								<input type="hidden" value="{{ csrf_token() }}" name="_token">
								@if(isset($record))
									<input type="hidden" value="{{ $record->id }}" name="id">
								@endif
								<div class="form-body">
									<div class="form-group">
										<input type="file" id="input-photo" class="{{ isset($record) && $record->photo ?: 'required-field' }}" data-field="Photo" name="photo" style="display: none" onchange="javascript:ValidateSingleInput(this)">
										<label class="control-label">Photo</label> <br>
										<a href="javascript:void(0)" class="btn btn-sm" onclick="$('#input-photo').trigger('click')">Browse</a> <span id="filename">{{ isset($record) && $record->photo ? $record->original_filename : '' }}</span>
									</div>
									<div class="form-group">
										<label class="control-label">Title</label>
										<input type="text" class="form-control required-field" data-field="Title" name="title" value="{{ isset($record) ? $record->title : '' }}">
									</div>
									<div class="form-group">
										<label class="control-label">Description</label>
										<textarea id="summernote_1" name="description" class="required-field" data-field="Description">{{ isset($record) ? $record->description : '' }}</textarea>
									</div>
									<div class="form-group">
										<label class="control-label">Mechanics</label>
										<textarea id="summernote_2" name="mechanics" class="required-field" data-field="Mechanics">{{ isset($record) ? $record->mechanics : '' }}</textarea>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn blue">Submit</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>

	<!-- MODAL -->
	<div id="invalid-file-type" class="modal fade" tabindex="-1" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Oops!</h4>
				</div>
				<div class="modal-body">
					 The file type you are trying to upload is invalid. Only <strong>JPG, JPEG, BMP, GIF, PNG</strong> are allowed.
				</div>
				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
	</div>

	<div id="previous-contests" class="modal fade" tabindex="-1" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Oops!</h4>
				</div>
				<div class="modal-body">
					 <table class="table table-bordered table-hover">
					 	<thead>
					 		<th>Title</th>
					 		<th>Date</th>
					 	</thead>
					 	@if(unserialize($record->previous))
						 	@forelse(unserialize($record->previous) as $key => $value)
						 		<tr>
						 			<td>{{ $value['title'] }}</td>
						 			<td>{{ $value['updated_at'] }}</td>
						 		</tr>
						 	@empty
						 		<tr>
						 			<td colspan="10"><center>No prevoius contest</center></td>
						 		</tr>
						 	@endforelse
						@else
						<tr>
				 			<td colspan="10"><center>No prevoius contest</center></td>
				 		</tr>
					 	@endif

					 </table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
	</div>
	<!-- MODAL -->
@endsection

@section('scripts')
<script src="{{ asset('assets_admin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets_admin/global/plugins/bootstrap-daterangepicker/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets_admin/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
	var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];
	var max_limit = 2145728;
	var error_container = $(".alert");
	var error_message_container = $(".alert>strong");
	function ValidateSingleInput(oInput) {
	    if (oInput.type == "file") {
	    	var elem = document.getElementById("input-photo");
	        var sFileName = oInput.value;
	        console.log(elem.files[0].size);
	         if (sFileName.length > 0) {
	            var blnValid = false;
	            for (var j = 0; j < _validFileExtensions.length; j++) {
	                var sCurExtension = _validFileExtensions[j];
	                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
	                    blnValid = true;
	                    break;
	                }
	            }

	            if(elem.files[0].size > max_limit) {
	            	error_container.show();
	            	error_message_container.html("Sorry, the allowed file size limit is 2MB.");
	            	elem.value = '';
	            	$('#filename').html('');
	            	return false;
	            }
	            if (!blnValid) {
	            	$('#invalid-file-type').modal('show');
	                return false;
	            }
	        }
	    }
	    var fullPath = document.getElementById('input-photo').value;
		if (fullPath) {
			var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
			var filename = fullPath.substring(startIndex);
			if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
				filename = filename.substring(1);
			}
			$('#filename').html(filename);
		}
	    return true;
	}
</script>
@endsection