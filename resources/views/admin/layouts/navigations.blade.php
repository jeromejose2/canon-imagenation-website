<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<ul class="page-sidebar-menu page-sidebar-menu-light " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
			<li class="sidebar-toggler-wrapper">
				<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				<div class="sidebar-toggler">
				</div>
				<!-- END SIDEBAR TOGGLER BUTTON -->
			</li>

			<!-- BEGIN NAVIGATION SIDE BAR -->
			<li class="start {{ Request::segment(2) == 'dashboard' ? 'active' : '' }}">
				<a href="{{ url('admin/dashboard') }}">
				<i class="fa fa-home"></i>
				<span class="title">Dashboard</span>
				</a>
			</li>
			<li class="{{ Request::segment(2) == 'users' ? 'active' : '' }}">
				<a href="{{ url('admin/users') }}">
				<i class="fa fa-users"></i>
				<span class="title">Users</span>
				</a>
			</li>
			<li class="{{ Request::segment(2) == 'user_photos' ? 'active' : '' }}">
				<a href="{{ url('admin/user_photos') }}">
				<i class="fa fa-image"></i>
				<span class="title">Photos</span>
				</a>
			</li>
			<li class="{{ Request::segment(2) == 'feeds' ? 'active' : '' }}">
				<a href="{{ url('admin/feeds') }}">
				<i class="fa fa-newspaper-o"></i>
				<span class="title">Articles</span>
				</a>
			</li>
			<li class="{{ Request::segment(2) == 'contest' ? 'active' : '' }}">
				<a href="{{ url('admin/contest') }}">
				<i class="fa fa-newspaper-o"></i>
				<span class="title">Contest</span>
				</a>
			</li>
			<li class="{{ Request::segment(2) == 'subscribers' ? 'active' : '' }}">
				<a href="{{ url('admin/subscribers') }}">
				<i class="fa fa-newspaper-o"></i>
				<span class="title">Subscribers</span>
				</a>
			</li>
			<li class="{{ Request::segment(2) == 'logs' ? 'active' : '' }}">
				<a href="{{ url('admin/logs') }}">
				<i class="fa fa-newspaper-o"></i>
				<span class="title">Admin Activities</span>
				</a>
			</li>
			<li class="last {{ Request::segment(2) == 'account_settings' ? 'active' : '' }}">
				<a href="javascript:void(0)">
				<i class="fa fa-user"></i>
				<span class="title">My Account</span>
				<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
					<li>
						<a href="{{ url('admin/account/settings') }}">
						Settings</a>
					</li>
					<li>
						<a href="{{ url('admin/logout') }}">
						Log Out</a>
					</li>
				</ul>
			</li>
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
</div>