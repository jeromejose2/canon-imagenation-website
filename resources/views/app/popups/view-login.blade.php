<div id="login-popup" class=" cf">
	<div class="inner-wrap cf">
		<div class="heading">
			Login with Facebook
		</div>

		<p>Connect your Facebook account</p>
		<a href="{{ url('facebook/authenticate') }}" class="customFbBtn" target="_blank"></a>

		<div class="cancel-wrap">
			<a href=""><em>Cancel</em></a>
		</div>
	</div>
</div>
