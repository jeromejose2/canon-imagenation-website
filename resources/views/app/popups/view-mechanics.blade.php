<div class="popups popup-large">
    <div class="popup-content-wrapper">
        <a href="javascript:void(0)" class="close-popup">X</a>
        <div class="content ">
            <h1 class="title">
                COMPLETE MECHANICS
             </h1>
            <div class="scroll-content">
                {!! isset($mechanics) ? $mechanics->mechanics : '' !!}
                {{-- <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore itaque cupiditate blanditiis ratione expedita laborum debitis, nisi? Iusto repellat, perferendis aspernatur architecto fugit aut consequuntur consectetur dolorem, nostrum nisi amet.
                </p>
                <br/>
                <h4 class="subtitle">
                    I. Lorem Ipsum
              </h4>
                <br/>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis laborum sequi alias ea enim. Ab voluptas est perferendis illum nesciunt non quaerat voluptatibus, dicta deleniti ratione error fugit, dolor id!
                </p>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore itaque cupiditate blanditiis ratione expedita laborum debitis, nisi? Iusto repellat, perferendis aspernatur architecto fugit aut consequuntur consectetur dolorem, nostrum nisi amet.
                </p>
                <br/>
                <h4 class="subtitle">
                    I. Lorem Ipsum
              </h4>
                <br/>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis laborum sequi alias ea enim. Ab voluptas est perferendis illum nesciunt non quaerat voluptatibus, dicta deleniti ratione error fugit, dolor id!
                </p>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore itaque cupiditate blanditiis ratione expedita laborum debitis, nisi? Iusto repellat, perferendis aspernatur architecto fugit aut consequuntur consectetur dolorem, nostrum nisi amet.
                </p>
                <br/>
                <h4 class="subtitle">
                    I. Lorem Ipsum
              </h4>
                <br/>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis laborum sequi alias ea enim. Ab voluptas est perferendis illum nesciunt non quaerat voluptatibus, dicta deleniti ratione error fugit, dolor id!
                </p>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore itaque cupiditate blanditiis ratione expedita laborum debitis, nisi? Iusto repellat, perferendis aspernatur architecto fugit aut consequuntur consectetur dolorem, nostrum nisi amet.
                </p>
                <br/>
                <h4 class="subtitle">
                    I. Lorem Ipsum
              </h4>
                <br/>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis laborum sequi alias ea enim. Ab voluptas est perferendis illum nesciunt non quaerat voluptatibus, dicta deleniti ratione error fugit, dolor id!
                </p>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore itaque cupiditate blanditiis ratione expedita laborum debitis, nisi? Iusto repellat, perferendis aspernatur architecto fugit aut consequuntur consectetur dolorem, nostrum nisi amet.
                </p>
                <br/>
                <h4 class="subtitle">
                    I. Lorem Ipsum
              </h4>
                <br/>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis laborum sequi alias ea enim. Ab voluptas est perferendis illum nesciunt non quaerat voluptatibus, dicta deleniti ratione error fugit, dolor id!
                </p>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore itaque cupiditate blanditiis ratione expedita laborum debitis, nisi? Iusto repellat, perferendis aspernatur architecto fugit aut consequuntur consectetur dolorem, nostrum nisi amet.
                </p>
                <br/>
                <h4 class="subtitle">
                    I. Lorem Ipsum
              </h4>
                <br/>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis laborum sequi alias ea enim. Ab voluptas est perferendis illum nesciunt non quaerat voluptatibus, dicta deleniti ratione error fugit, dolor id!
                </p> --}}
            </div>
            <div id="upload" class="upload-call2action-wrap" style="height: 0px; background-color: none;">
              <!-- <span style="position:absolute;top:0;left:0;">request for pattern;</span> -->

                  <div class="outer-container">

                    <a id="btn-upload-close" href="javascript:void(0)" class="mybtn graystyle customBtn">
                      <span class="upperLayer customBtn">UPLOAD A PHOTO</span>
                    </a>

                  </div>
            <!--end of upload-->
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .upload-call2action-wrap:after, .upload-call2action-wrap:before {
        background: none;
    }
</style>

<script type="text/javascript">
$(function() {
    var magnificPopup = $.magnificPopup.instance; //
    var uploadBtn = $("#btn-upload");
    $("#btn-upload-close").on("click", function() {
        magnificPopup.close();
        console.log(uploadBtn);
        setTimeout(function() {
            uploadBtn.trigger("click");
        }, 100);
    });
    $('.close-popup').on('click', function() {
        magnificPopup.close();
    });
    $('.scroll-content').slimscroll({
        height: '450px',
        size: '5px',
        distance: '5px',
            alwaysVisible: true,

        color:'#e01b23',
        railOpacity: 1.0

    });


})
</script>
