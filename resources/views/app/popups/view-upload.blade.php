<div class="popups popup-large">
    <div class="popup-content-wrapper">
        <a href="javascript:void(0)" class="close-popup">X</a>
        <div class="content ">
            <h1 class="title">
               UPLOAD PHOTO
             </h1>
            <p class="upload-subtitle">Hello! You may now upload a photo of your choice. Kindly fill out this form to continue.</p>
            <ul class="error-message">
            </ul>
            <h1 class="bold">
                Basic Information <span class="asterisk">*</span>
            </h1>
            <div class="forms">
                <form action="{{ url('submit-entry') }}" method="POST" enctype="multipart/form-data" onsubmit="return validateUploadForm()">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="text" class="photo-input required" placeholder="Photo title" name="title" data-field="Photo title">
                    <textarea placeholder="Short description" class="description required" name="description" data-field="Short description"></textarea>
                    <input type="text" class="photo-input size40p required" placeholder="Camera Used" name="camera" data-field="Camera Used">
                    <input type="text" class="photo-input size20p required" placeholder="ISO" name="settings_iso" data-field="ISO">
                    <input type="text" class="photo-input size20p required" placeholder="Aperture" name="settings_aperture" data-field="Aperture">
                    <input type="text" class="photo-input size20p required" placeholder="Shutter Speed" name="settings_shutter_speed" data-field="Shutter Speed">
                    <h1 class="bold lbl-category">
                        Select Photo Category <span class="asterisk">*</span>
                    </h1>
                    <div class="category-wrap">
                        <select name="category" class="required" data-field="Photo Category">
                            <option value="">Please select a category ....</option>
                            @if(isset($categories) && $categories)
                                @foreach($categories as $category)
                                    <option>{{ $category->content }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <h1 class="bold">
                        Attachment<span class="asterisk">*</span>
                    </h1>
                    <div class="choosefile-wrap">
                        <div class="content cf">
                            <div class="btn-wrap" style="text-alignt:left;">
                                <div class="btn-red">
                                    <a href="javascript:;" class="inner" onclick="$('input[type=file]').trigger('click')">Choose File</a>
                                    <input type="file" id="input-file" class="required" data-field="Attach a Photo" name="files[]" multiple style="display: none" accept="image/jpg, image/jpeg" onchange="getFilenames()">
                                    <span style="position:relative;top:10px;font-size:14px;" class="name-cont"></span>
                                </div>
                            </div>
                            <div class="desc-wrap"  style="text-align:right;font-size:14px;">
                                <p>Upload the Web Version of your Photograph</p>
                                <p>Max Upload Size: <strong>3MB</strong> | File Format: <strong>.jpg</strong></p>
                            </div>
                        </div>
                    </div>
                    <div class="purpose cf">
                        <div class="lbl-left">
                            <h1 class="bold">
                             This photo is...<span class="asterisk">*</span>
                            </h1>
                        </div>
                        <div class="choices cf">
                            <label>
                                <input type="radio" name="type" value="{{ ENTRY }}" checked><span> An entry to the contest</span></label>
                            <label>
                                <input type="radio" name="type" value="{{ SHARING }}"><span> For sharing</span></label>
                        </div>
                    </div>
                    <div class="captcha-wrap cf">
                        <div class="captcha-content">
                            {{-- <img src="assets/img/captcha-dummy.jpg" alt=""> --}}
                            <div style="width:100%;" class="g-recaptcha" data-sitekey="6LepuBETAAAAAMnMLBe7Vc3kA5uqqwpJIgX87ZZ8"></div>
                        </div>
                    </div>
                    <div class="terms-wrap cf">
                        <label>
                            <input type="checkbox" name="terms_and_conditions" class="required" data-field="Terms and Conditions">
                            <span style="font-size:14px;">I agree to the given <strong><a href="#" class="view-reg-terms">Terms &amp; Conditions</a></strong></span>
                        </label>
                    </div>
                    <div class="btn-wrap">
                        <input type="submit" id="submit-btn" style="display: none">
                        <a href="javascript:void(0);" class="mybtn graystyle subscribeBtn" onclick="$('#submit-btn').trigger('click')">
                            <span class="upperLayer subscribeBtn">SUBMIT PHOTO</span>
                        </a>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <!-- Contents of second window -->
        <div id ="terms">
                    <div class="popup-content-wrapper">
                        <div class="content ">
                            <h1 class="title">
                                TERMS &amp; CONDITIONS
                             </h1>
                            <div class="scroll-content">
                                <p>
                                    Thank you for visiting the Canon Philippines website and welcome to Canon Marketing Philippines’ Terms and Conditions of Use Agreement (“Agreement”). This Website is the property of Canon Philippines and is maintained and made available to the public for the purpose of offering to those who are interested in Canon and Canon’s products and services a convenient way to find relevant information, such as the latest news releases, financial information, product lineup, local contact information and websites owned by Canon’s subsidiaries or its distributors around the world.
                                </p>
                            </div>
                        </div>
                    </div><br/>
                    <a href="#" class="hide-terms">GO BACK >></a>
        </div>
</div>

<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">
$(function() {
    var magnificPopup = $.magnificPopup.instance;

    $('.close-popup').on('click', function() {
        magnificPopup.close();
    });
    $('.scroll-content').slimscroll({
        height: '450px',
        size: '5px',
        distance: '5px',
            alwaysVisible: true,

        color:'#e01b23',
        railOpacity: 1.0


    });
    $('.view-reg-terms').on('click',function(){
            $('#terms').addClass('show-reg-terms');
    });

    $('.hide-terms').on('click',function(){
                    $('#terms').removeClass('show-reg-terms');

    });


//on ready end
});
</script>

<style>

.error-message {
    padding-left: 20px;
    border-left: 3px solid red;
    display: block;
    background-color: #ccc;
}

.error-message li {
    display: block;
}

</style>
