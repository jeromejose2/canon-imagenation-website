<div class="popups popup-large">
    <div class="popup-content-wrapper">
        <div class="content txtcenter">
		<h3>
			<b>Oops!</b>
		</h3><BR/>
		<span id="thank-you-content">
			Sorry, something went wrong with your submission!
		</span><br/><br/>
		<a href="{{ url('facebook/authenticate') }}" target="_blank" class="retry-submission close-popup mybtn graystyle subscribeBtn">
	          <span class="upperLayer subscribeBtn">RETRY</span>
	      </a>
        </div>
    </div>
</div>
<script type="text/javascript">
$(function() {
	// $('#thank-you-content').html(invalid_submission);

    var magnificPopup = $.magnificPopup.instance;
    $('.close-popup, .retry-submission').on('click', function() {
        magnificPopup.close();
    });
})
</script>