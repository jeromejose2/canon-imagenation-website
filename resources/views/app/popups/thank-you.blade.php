<div class="popups popup-large">
    <div class="popup-content-wrapper">
        <div class="content txtcenter">
		<h2>
			<b>Thank you!</b>
		</h2><BR/>
		<span style="font-size:14px;" id="thank-you-content">

		</span><br/><br/>
		<a href="javascript:void(0)" class="close-popup mybtn graystyle subscribeBtn">
	              <span class="upperLayer subscribeBtn">CONTINUE</span>
	          </a>
        </div>
    </div>
</div>
<script type="text/javascript">
$(function() {
	$('#thank-you-content').html(has_thankyou);

    var magnificPopup = $.magnificPopup.instance;
    $('.close-popup').on('click', function() {
        magnificPopup.close();
    });
})
</script>
