<div class="popups popup-large">
    <div class="popup-content-wrapper">
        <div class="content txtcenter">
		<h3>
			<b>Oops</b>
		</h3><BR/>
		<span id="subscription-error">

		</span><br/><br/>
		<a href="javascript:void(0)" class="close-popup mybtn graystyle subscribeBtn">
	              <span class="upperLayer subscribeBtn">CONTINUE</span>
	          </a>
        </div>
    </div>
</div>
<script type="text/javascript">
$(function() {
	$('#subscription-error').html(subscription_error);

    var magnificPopup = $.magnificPopup.instance;
    $('.close-popup').on('click', function() {
        magnificPopup.close();
    });
})
</script>
