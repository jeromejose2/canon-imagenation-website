<div class="popups popup-large">
    <div class="popup-content-wrapper">
        <a href="javascript:void(0)" class="close-popup">X</a>
        <div class="content ">
            <h1 class="title">
                TERMS &amp; CONDITIONS
             </h1>
            <div class="scroll-content">
                <p>
                    Thank you for visiting the Canon Philippines website and welcome to Canon Marketing Philippines’ Terms and Conditions of Use Agreement (“Agreement”). This Website is the property of Canon Philippines and is maintained and made available to the public for the purpose of offering to those who are interested in Canon and Canon’s products and services a convenient way to find relevant information, such as the latest news releases, financial information, product lineup, local contact information and websites owned by Canon’s subsidiaries or its distributors around the world.
                </p>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(function() {
    var magnificPopup = $.magnificPopup.instance; //
    $('.close-popup').on('click', function() {
        magnificPopup.close();
    });
    $('.scroll-content').slimscroll({
        height: '450px',
        size: '5px',
        distance: '5px',
            alwaysVisible: true,

        color:'#e01b23',
        railOpacity: 1.0


    });
})
</script>
