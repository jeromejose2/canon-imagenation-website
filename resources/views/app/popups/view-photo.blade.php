<div class="image-popup">
    <a href="javascript:void(0)" class="close-popup">X</a>
    <div class="image">
        <img class="img-responsive" alt="photo-zoom" src="{{ isset($photo) && $photo->photo ? asset('uploads').'/'.$photo->photo : '' }}" />
    </div>
    <div class="image-description">
            <h3>{{ isset($photo) ? $photo->title : '' }}</h3>
       <span class="desc-italic">Captured by: {{ isset($photo) ? $photo->full_name : '' }}</span> <span class="desc-italic">Under: {{ isset($photo) ? $photo->category : '' }}</span><br/>
       <p class="description">
           {{ isset($photo) ? $photo->description : '' }}
       </p><br/>
       <div class="settings">
            <ul>
                <li>
                    <i class="fa icon-sprite-camera3"></i>
                     <span>{{ isset($photo) ? $photo->camera : '' }}</span>
                </li>
                <li>
                    <i class="fa icon-sprite-eye110"></i>
                     <span>{{ isset($photo) ? @unserialize($photo->settings)['shutter_speed'] : '' }}</span>
                </li>
                <li>
                    <i class="fa icon-sprite-photo223"></i>
                     <span>{{ isset($photo) ? @unserialize($photo->settings)['aperture'] : '' }}</span>
                </li>
                <li>
                    <i class="fa icon-sprite-iso10"></i>
                     <span>{{ isset($photo) ? @unserialize($photo->settings)['iso'] : '' }}</span>
                </li>
            </ul>
       </div>
    </div>
</div>
<script type="text/javascript">
$(function() {
    var magnificPopup = $.magnificPopup.instance; //
    $('.close-popup').on('click', function() {
        magnificPopup.close();
    });
})
</script>
