@extends('app.layouts.master')

@section('content')
<div class="container">
    <div class="blog-content">
              <div class="btn-group btn-breadcrumb">
                  <a href="http://www.canonimagenation.ph" class="first-crumb btn btn-default"><i class="glyphicon glyphicon-home"></i></a>
                  <a href="http://www.canonimagenation.ph/blogs" class="active-parent-crumb  btn btn-default">Blogs</a>
              </div><br/><br/>
              <div class="blog-items">

              @if($blogs)
                @foreach($blogs as $blog)
                      <div class="blog-item">
                            <div class="item-content">
                               <h1>{{ $blog->title }}</h1>
                               <span>{{ date('F d, Y', strtotime($blog->date)) }}</span> | <span>{{ $blog->crusader }}</span>
                               <br/>
                               <img src="{{ asset('assets_admin/uploads/blog') }}/{{ $blog->photo }}" class="img-responsive"/>
                               <p>
                                  {!! str_limit($blog->description, 350, '...') !!}
                               </p>
                            </div>
                           <br/>
                           <div class="read-more">
                             <a href="{{ url('blogs/read') }}/{{ $blog->permalink }}">READ MORE</a>
                           </div>
                    </div>
                @endforeach
              @endif


              <?php /*
                <?php for($i = 0;$i < 10;$i ++){ ?>
                  <div class="blog-item">
                          <div class="item-content">
                             <h1>Canon PowerShot G5 X</h1>
                             <span>December 01, 2015 </span> | <span>Name of the Crusader</span>
                             <br/>
                             <img src="assets/img/blog-image.png" class="img-responsive"/>
                             <p>

                                The Canon PowerShot G5 X is a new premium compact camera with a large CMOS image sensor and a fast lens. he metal-bodied G5 X has a 1.0-type back-illuminated 20.2 Megapixel CMOS sensor, an f/1.8-2.8, 4.2x lens with 9-blade aperture, full manual controls, shooting mode and exposure compensation dials, built-in OLED EVF with 2,360K dots, 3-inch tilting LCD touchscreen with 1.04million dots.
                             </p>
                          </div>
                         <br/>
                         <div class="read-more">
                           <a href="blog-inner.php">READ MORE</a>
                         </div>
                  </div>
                <?php } ?>
              <?php */ ?>
              </div>

    </div>

</div>

    <div class="horizontal-lapses">
          <a href="" class="btn scrollup"></a>
    </div>

    <div id="subscribe" class="cf">
        <h2 class="label">Subscribe for Updates!</h2>
        <h5 style="color: red" class="subscribe-error"></h5>
        <div class="info-wrap">
              <form id="form-subscribe" action="{{ url('subscribe') }}" method="POST" onsubmit="return validateSubscribeForm()">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="text" class="custom subscribe-required" placeholder="First Name" name="subscribe_firstname">
                  <input type="text" class="custom subscribe-required" placeholder="Last Name" name="subscribe_lastname">
                  <input type="text" class="custom subscribe-required" placeholder="Email Address" name="subscribe_email">
              </form>
        </div>

        <div class="btn-wrap cf">
          <a href="javascript:submitSubscribeForm();" class="mybtn graystyle subscribeBtn">
              <span class="upperLayer subscribeBtn">SUBSCRIBE</span>
          </a>
        </div>
    </div>
@endsection
