@extends('app.layouts.master')

@section('content')
<!-- <section> -->
    <div class="banner"></div>

    <div id="feature" class="gallery-container cf">

        <div class="gallery-content  cf">
                    <!-- JOIN THE NATION -->
                   <div class="join-nation  col-md-6 cf">
                        <div class="title cf">
                                <h1>join the nation</h1>
                        </div>

                        <div class="img-wrap cf">

                            <img src="{{ isset($contest->photo) && $contest->photo ? asset('assets_admin/uploads/contest').'/'.$contest->photo : '' }}" class="img-responsive" alt="join-nation">

                            <div class="img-description cf">
                                  <h1 class="heading">{!! isset($contest->title) ? $contest->title : '' !!}</h1>
                                  <p>{!! isset($contest->description) ? $contest->description : '' !!}</p>
                            </div>
                        </div>

                      <div class="btn-wrap cf">
                        <a href="javascript:void(0)" class="view-mechanics mybtn graystyle">
                          <span class="upperLayer">VIEW MECHANICS</span>
                        </a>
                      </div>

                   </div>

                   <!-- JUST IN -->
                   <div class="just-in col-md-6 cf">
                        <div class="title cf">
                                <h1>just in</h1>
                        </div>
                        <div id="feeds-container">
                            <ul class="feed-content cf">
                                @if(isset($feeds) && $feeds)
                                    @foreach($feeds as $feed)
                                        <li>
                                          <a href="{{ url('blogs/read') }}/{{ $feed->permalink }}" target="_blank"><h1 class="txt-gray inner-title">{{ $feed->title }}</h1></a>
                                          <p>{!! str_limit(strip_tags($feed->description), 150, '...') !!}</p>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                            @if($feeds->lastPage() > 1)
                              <div class="pagination-wrap cf">
                                    <div class="older-wrap">
                                      <a href="javascript:void(0)" onclick="paginateFeeds('{{ ($feeds->currentPage() == 1) ? false : url('ajax/feeds').'?page=1'}}')">Older</a>
                                    </div>
                                    <div class="number-wrap">
                                        <ul>
                                          @for($feeds_counter = 1; $feeds_counter <= $feeds->lastPage(); $feeds_counter++)
                                              <li><a href="javascript:void(0)" class="{{ ($feeds->currentPage() == $feeds_counter) ? 'active' : '' }}" onclick="paginateFeeds('{{ url('ajax/feeds').'?page='.$feeds_counter }}')">{{ $feeds_counter }}</a></li>
                                          @endfor
                                        </ul>
                                    </div>
                                    <div class="newer-wrap cf">
                                      <a href="javascript:void(0)" onclick="paginateFeeds('{{ ($feeds->currentPage() == $feeds->lastPage()) ? false : url('ajax/feeds').'?page='.($feeds->currentPage() + 1) }}')">Newer</a>
                                    </div>
                              </div>
                            @endif
							<br/>
							   <div class="btn-wrap txtcenter cf">
								<a href="{{ url('blogs') }}"  class="  mybtn graystyle">
								  <span class="upperLayer">VIEW BLOG</span>
								</a>
							  </div>
                        </div>
                   </div>



            <!-- end of gallery-content -->
            </div>
    <!-- end of gallery-container -->
    </div>


    <div id="upload" class="upload-call2action-wrap">
      <!-- <span style="position:absolute;top:0;left:0;">request for pattern;</span> -->

          <div class="outer-container">
            <span class="caption">Capture stories worth sharing.</span>

            <a id="btn-upload" href="javascript:void(0)" class="mybtn graystyle customBtn">
              <span class="upperLayer customBtn">UPLOAD A PHOTO</span>
            </a>

          </div>
    <!--end of upload-->
    </div>

    <div id="showcase" class="outer-container cf">
          <div class="container cf">

              <div class="title cf">

                <h1 class="bold atMobile">Visual Storytellers Showcase</h1>

                <div class="category-wrap">

                  <select onchange="paginatePhotos('{{ url('ajax/photos').'?page=1' }}')">
                    <option value="">Filter photos by</option>
                    @if(isset($categories) && $categories)
                        @foreach($categories as $category)
                          <option>{{ $category->content }}</option>
                        @endforeach
                    @endif
                  </select>

                </div>

            </div>

            <!-- AJAX CONTAINER -->
            <div id="photo-ajax-container">

                <div class=" grid cf">
                      @forelse($photos as $photo)
                         @if(file_exists('./uploads/'.$photo->photo))
                            <div class="lazyloading grid-item cf" >
                                  <a class="view-photo" href="{{ action('PopupController@photo', ['id'=>$photo->id]) }}">
                                       <img data-src ="{{ asset('uploads').'/thumb_'.$photo->photo }}" class="lazy img-responsive"/>
                                  </a>
                           </div>
                          @endif
                      @empty
                        <center>No result found</center>
                      @endforelse
                </div>

                  <div class="pagination-wrap cf">

                      <div class="older">
                        <a href="javascript:void(0)" class="prev-caret" onclick="paginatePhotos('{{ ($photos->currentPage()) == 1 ? false : url('ajax/photos').'?page=1' }}')"></a>
                        Older
                      </div>


                      <div class="numbers">

                          <ul>
                            <li>
                                @for($i = 1; $i <= $photos->lastPage(); $i++)
                                    <li><a href="javascript:void(0)" class="{{ ($photos->currentPage() == $i) ? 'active' : '' }}" onclick="paginatePhotos('{{ url('ajax/photos').'?page='.$i }}')">{{ $i }}</a></li>
                                @endfor
                            </li>

                          </ul>

                      </div>

                      <div class="newer">
                        Newer
                        <a href="javascript:void(0)" class="next-caret" onclick="paginatePhotos('{{ ($photos->currentPage() == $photos->lastPage()) ? false : url('ajax/photos').'?page='.($photos->currentPage() + 1) }}')"></a>
                      </div>
                  <!--pagination-wrap-->
                  </div>

          </div>
          <!-- AJAX CONTAINER -->

          </div>
    <!--end of showcase-->
    </div>


    <div class="horizontal-lapses">
          <a href="javascript:void(0)" class="btn scrollup"></a>
    </div>

    <div id="subscribe" class="cf">
        <h2 class="label">Subscribe for Updates!</h2>
        <h5 style="color: red" class="subscribe-error"></h5>
        <div class="info-wrap">
              <form id="form-subscribe" action="{{ url('subscribe') }}" method="POST" onsubmit="return validateSubscribeForm()">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="text" class="custom subscribe-required" placeholder="First Name" name="subscribe_firstname" data-fieldname="Firstname">
                  <input type="text" class="custom subscribe-required" placeholder="Last Name" name="subscribe_lastname" data-fieldname="Lastname">
                  <input type="text" class="custom subscribe-required" placeholder="Email Address" name="subscribe_email" data-fieldname="Email Address">
              </form>
        </div>

        <div class="btn-wrap cf">
          <a href="javascript:void(0)" onclick="submitSubscribeForm();" class="mybtn graystyle subscribeBtn">
              <span class="upperLayer subscribeBtn">SUBSCRIBE</span>
          </a>
        </div>
    </div>

  <input type="button" id="thank-you" style="display: none">
  <input type="button" id="invalid-submission" style="display: none">
  <input type="button" id="btn-upload-with-existing-user" style="display: none">
  <input type="button" id="subscription-error" style="display: none">
<!-- </section> -->
@endsection
