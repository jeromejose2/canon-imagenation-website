@extends('app.layouts.master')

@section('content')
<div class="container">
    <div class="blog-content">
              <div class="btn-group btn-breadcrumb">
                  <a href="http://www.canonimagenation.ph" class="first-crumb btn btn-default"><i class="glyphicon glyphicon-home"></i></a>
                  <a href="http://www.canonimagenation.ph/blogs" class="active-parent-crumb  btn btn-default">Blogs</a>
                  <a href="javascript:void(0)" class="btn btn-default blog-last-child">{{ $blog->title }}</a>
              </div><br/><br/>
              <div class="blog-items">


                  <div class="blog-item">
                          <div class="item-content">
                             <h1>{{ $blog->title }}</h1>
                             <span>{{ date('F d, Y', strtotime($blog->date)) }}</span> | <span>{{ $blog->crusader }}</span>
                             <br/>
                             <img src="{{ asset('assets_admin/uploads/blog') }}/{{ $blog->photo }}" class="img-responsive"/>
                             <p>
                                {!! $blog->description !!}
                             </p>
                          </div>
                         <br/>
                         <div class="read-more txtcenter">
                           <ul>
                               <li><span class="share-data" data-title="{{ $blog->title }}" data-description="{!! str_limit(strip_tags($blog->description), 100, '...') !!}" data-image="{{ asset('assets_admin/uploads/blog') }}/{{ $blog->photo }}" data-twitter-share="{{ $blog->short_url }}" data-url="{{ Request::url() }}">SHARE: </span></li>
                               <li><a href="javascript:void(0)" class="icon icon-sprite-facebook3" id="share-facebook">facebook</a></li>
                               <li><!--<a href ="javascript:void(0)" class="icon icon-sprite-twitter" id="share-twitter">twitter</a>--><a class="twitter-share-button"
  href="https://twitter.com/intent/tweet">
Tweet</a></li>
                           </ul>
                         </div>
                  </div>

              </div>
              <div class="pagination-blog clearfix">
                  <a href="{{ url('blogs') }}" class="pull-left">GO BACK</a>
                  <a href="{{ url() }}" class="pull-right">GO TO HOME</a>

              </div>

    </div>

</div>

    <div class="horizontal-lapses">
          <a href="" class="btn scrollup"></a>
    </div>

    <div id="subscribe" class="cf">
        <h2 class="label">Subscribe for Updates!</h2>
        <h5 style="color: red" class="subscribe-error"></h5>
        <div class="info-wrap">
              <form id="form-subscribe" action="{{ url('subscribe') }}" method="POST" onsubmit="return validateSubscribeForm()">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="text" class="custom subscribe-required" placeholder="First Name" name="subscribe_firstname">
                  <input type="text" class="custom subscribe-required" placeholder="Last Name" name="subscribe_lastname">
                  <input type="text" class="custom subscribe-required" placeholder="Email Address" name="subscribe_email">
              </form>
        </div>

        <div class="btn-wrap cf">
          <a href="javascript:submitSubscribeForm();" class="mybtn graystyle subscribeBtn">
              <span class="upperLayer subscribeBtn">SUBSCRIBE</span>
          </a>
        </div>
    </div>
@endsection
