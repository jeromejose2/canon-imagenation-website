<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Canon Imagenation</title>

    @if(isset($blog))
        <meta property="fb:app_id" content="1062847087072425">
        <meta property="og:title" content="{{ $blog->title}}">
        <meta property="og:url" content="{{ Request::url() }}">
        <meta property="og:image" content="{{ asset('assets_admin/uploads/blog') }}/{{ $blog->photo }}">
        <meta property="og:description" content="{!! str_limit(strip_tags($blog->description), 100, '...') !!}">

        {{-- <meta name="twitter:card" content="photo">
        <meta name="twitter:site" content="@nuworksQA">
        <meta name="twitter:title" content="{{ @$blog->title }}">
        <meta name="twitter:image" content="{{ asset('assets/img/twitter-share-test-image.jpg') }}">--}}
    @endif

    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.png') }}"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body>
    <!--[if lt IE 10]>
  <p class="browsehappy">
    You are using an <strong>outdated</strong> browser. Please
    <a href="http://browsehappy.com/">upgrade your browser</a> to improve
    your experience.
  </p>
<![endif]-->
<header>
    <div class="pre-header">
         <div class="container">
                <div class="socials">
                    <ul>
                        <li>
                            <a href="https://www.facebook.com/canonphils" target="_blank"><i class="fa fa-facebook"></i></a>
                        </li>
                         <li>
                            <a href="https://twitter.com/canonphils" target="_blank"><i class="fa fa-twitter"></i></a>
                        </li>
                         <li>
                            <a href="https://www.instagram.com/canonphils/" target="_blank"><i class="fa fa-instagram"></i></a>
                        </li>
                    </ul>
                </div>

        </div>
    </div>
    <div class="container header-banner-logo">
            <div class="canon-logo">
                    <a href="{{action('HomeController@index')}}"><img src="{{ asset('assets/img/canon-logo.png') }}" class="img-responsive"/></a>
            </div>
            <div class="imagenation-logo">
                    <img src ="{{ asset('assets/img/imagenation-banner.png') }}" class="img-responsive"/>
            </div>
    </div>

</header>


