   <footer>
        <div class="container">
          <div class="half sixty">
              Copyright &copy; 2011 - 2015 Canon Marketing (Phils) Inc. ImageNation. All Rights Reserved 2015.
          </div>
          <div class="half forthy">
            <a href="javascript:void(0)" class="trigger-terms">Terms &amp; Conditions</a>
            <span class="vertical-line"></span>

            {{-- <a href="">Privacy Policy</a>
            <span class="vertical-line"></span> --}}

            <a href="http://www.canon.com.ph" class="" target="_blank">Canon Official Website</a>

            <!-- Terms &amp; Conditions  |  Privacy Policy   |  Canon Official Website -->
          </div>
        </div>
   </footer>

   <script>
    var base_url = "{{ url() }}/";
    var has_thankyou = "{{ Session::has('has_thankyou') ? Session::get('has_thankyou') : '' }}";
    var invalid_submission = "{{ Session::has('invalid_submission') ? Session::get('invalid_submission') : '' }}";
    var subscription_error = "{{ Session::has('subscription_error') ? Session::get('subscription_error') : '' }}";
   </script>

    <?php
     /*<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="{{ asset('assets/vendors/magnifico/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/2.2.2/isotope.pkgd.min.js"></script>
    <script src="{{ asset('assets/js/main.min.js') }}"></script> */
    ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="{{ asset('assets/vendors/magnifico/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('assets/vendors/slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('assets/vendors/isotopes.min.js')}}"></script>
    <script src="{{ asset('assets/vendors/jquery.lazyload.min.js')}}"></script>
    <script src="{{ asset('assets/vendors/imagesloaded.js')}}"></script>
    <script src="{{ asset('assets/js/main.min.js') }}"></script>
    <script src="{{ asset('assets/js/backend.js') }}?dev={{ rand() }}"></script>
    <script type="text/javascript">
            $(document).ready(function () {
               $('.grid').imagesLoaded(function () {
                    var selector = '.grid';
                    $(selector).isotope({
                        itemSelector: '.grid-item',
                        masonry: {

                        }
                    });

                    $(".lazy").lazyload({
                        container: $(".grid")
                    });
                    $(selector).isotope('layout');

                });


                $.each($('img'), function (idx, item) {
                    var src = $(this).data('src');
                    $(item).attr('src',src)
                })
        });
        $(".view-photo").magnificPopup({type:"ajax",showCloseBtn:!1,preloader:!0});
  </script>
  @yield('scripts')
</body>

</html>
