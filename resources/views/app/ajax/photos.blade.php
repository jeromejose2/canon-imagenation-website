<div class="grid cf">
      @forelse($photos as $photo)
          @if(file_exists('./uploads/'.$photo->photo))
            <div class="lazyloading grid-item cf" >
                    <a class="view-photo" href="{{ action('PopupController@photo', ['id'=>$photo->id]) }}" >

                         <img data-src ="{{ asset('uploads').'/thumb_'.$photo->photo }}" class="lazy img-responsive"/>
                    </a>
             </div>
           @endif
      @empty
        <center>No result found.</center>
      @endforelse
  </div>

  <div class="pagination-wrap cf">
      <div class="older">
        <a href="javascript:void(0)" class="prev-caret" onclick="paginatePhotos('{{ ($photos->currentPage()) == 1 ? false : url('ajax/photos').'?page=1' }}')"></a>
        Older
      </div>

      <div class="numbers">
              <ul>
                <li>
                    @for($i = 1; $i <= $photos->lastPage(); $i++)
                        <li><a href="javascript:void(0)" class="{{ ($photos->currentPage() == $i) ? 'active' : '' }}" onclick="paginatePhotos('{{ url('ajax/photos').'?page='.$i }}')">{{ $i }}</a></li>
                    @endfor
                </li>

              </ul>
          </div>

      <div class="newer">
        Newer
        <a href="javascript:void(0)" class="next-caret" onclick="paginatePhotos('{{ ($photos->currentPage() == $photos->lastPage()) ? false : url('ajax/photos').'?page='.($photos->currentPage() + 1) }}')"></a>
      </div>
  <!--pagination-wrap-->
  </div>

<script>
  $(document).ready(function () {


               $('.grid').imagesLoaded(function () {
                    var selector = '.grid';
                    $(selector).isotope({
                        itemSelector: '.grid-item',
                        masonry: {

                        }
                    });

                    $(".lazy").lazyload({
                        container: $(".grid")
                    });
                    $(selector).isotope('layout');

                });


                $.each($('img'), function (idx, item) {
                    var src = $(this).data('src');
                    $(item).attr('src',src)
                })

        });
        $(".view-photo").magnificPopup({type:"ajax",showCloseBtn:!1,preloader:!0});

</script>
