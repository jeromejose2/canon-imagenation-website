<ul class="feed-content cf">
    @if(isset($feeds) && $feeds)
        @foreach($feeds as $feed)
            <li>
              <a href="{{ url('blogs/read') }}/{{ $feed->permalink }}" target="_blank"><h1 class="txt-gray inner-title">{{ $feed->title }}</h1></a>
              <p>{!! str_limit($feed->description, 150, '...') !!}</p>
            </li>
        @endforeach
    @endif
</ul>
<div class="pagination-wrap cf">
      <div class="older-wrap">
        <a href="javascript:void(0)" onclick="paginateFeeds('{{ ($feeds->currentPage() == 1) ? false : url('ajax/feeds').'?page=' }}{{ ($feeds->currentPage() == 1) ? false : ($feeds->currentPage() == 1 ? '1' : ($feeds->currentPage() - 1)) }}')">Older</a>
      </div>
      <div class="number-wrap">
          <ul>
            @for($feeds_counter = 1; $feeds_counter <= $feeds->lastPage(); $feeds_counter++)
              <li><a href="javascript:void(0)" class="{{ ($feeds->currentPage() == $feeds_counter) ? 'active' : '' }}" onclick="paginateFeeds('{{ url('ajax/feeds').'?page='.$feeds_counter }}')">{{ $feeds_counter }}</a></li>
            @endfor
          </ul>
      </div>
      <div class="newer-wrap cf">
        <a href="javascript:void(0)" onclick="paginateFeeds('{{ ($feeds->currentPage() == $feeds->lastPage()) ? false : url('ajax/feeds').'?page='.($feeds->currentPage() + 1) }}')">Newer</a>
      </div>
</div>
<div class="btn-wrap txtcenter cf">
  <a href="{{ url('blogs') }}"  class="  mybtn graystyle">
    <span class="upperLayer">VIEW BLOG</span>
  </a>
</div>